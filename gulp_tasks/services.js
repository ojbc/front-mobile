const gulp = require('gulp');
const gulpNgConfig = require('gulp-ng-config');

const conf = require('../conf/gulp.conf');

gulp.task('services', services);

function services() {
  return gulp.src(conf.path.const('services_url.json'))
    .pipe(gulpNgConfig('build.services'))
    .pipe(gulp.dest(conf.path.const('build')));
}