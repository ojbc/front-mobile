(function(){
    'use strict';
    
    angular
      .module('app')
        .config(configRoutes)
        .config(stateRoutes);

    /** @ngInject */
    function configRoutes($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }

    /** @ngInject */ 
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
        $urlRouterProvider.otherwise('/home');

        $stateProvider
          .state('app', {
            url: '/',
            component: 'app',
            resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
          });
    }

    configRoutes.$inject = [ '$qProvider' ];

    stateRoutes.$inject = [ 
        '$stateProvider', 
        '$urlRouterProvider', 
        '$locationProvider' 
    ];

})();

/** @ngInject */
function _skipIfAuthenticated($q, $location, $auth) {
    if( $auth.isAuthenticated() ){
        //
      
        $location.path('/home');
    }
}

/** @ngInject */
function _redirectIfNotAuthenticated($q, $location, $auth) {
    if(! $auth.isAuthenticated() ){
        //
        $location.path('/login');
    }
}

/** @ngInject */
function fnThatReturnsAPromise($q) {
    var defer = $q.defer();
    defer.resolve(false);
    return defer.promise;
}

/** @ngInject */
/*function _checkPermission($location, localStorageService) {
    
    return {
        validate: function(allowed) {
            var permissions = localStorageService.get('auth_permission');
            var permission = permissions.find(function(item) {
                return item.slug === allowed;
            });
            
            if (!permission) {
                $location.path('/home');
            } 

        }
    }
}*/