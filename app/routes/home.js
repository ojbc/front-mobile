(function(){
    'use strict';

	angular
	  .module('app')
		.config(stateRoutes)
		.run(loadTransitions);

	/** @ngInject */
	function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
		$stateProvider
	    .state('app.home', {
	    	url: 'home',
	      	component: 'home',
	      	resolve: { 
                redirectIfNotAuthenticated: _redirectIfNotAuthenticated 
            },
		})
		.state('app.association', {
			url: 'association',
			component: 'association',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {

                    return $ocLazyLoad.load('AssociationModule');
                }] },
			data: {
				permissions: {
					only: 'superadmin',
					redirectTo: 'app.home'
				}
			}
		})
		.state('app.admin', {
			url: 'superadmin',
			component: 'company',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {

                    return $ocLazyLoad.load('CompanyModule');
                }] },
			data: {
				permissions: {
				only: 'superadmin',
				redirectTo: 'app.home'
				}
			}
		})
		.state('app.dt', {
			url: 'delete-transaction',
			component: 'dt',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {

                    return $ocLazyLoad.load('DeleteTransactionModule');
                }] },
			data: {
				permissions: {
					only: 'superadmin',
					redirectTo: 'app.home'
				}
			}
		})
		.state('app.importing-data-bank', {
			url: 'importing-data-bank',
			component: 'idb',
			resolve: {
			  redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
			  loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {

				  return $ocLazyLoad.load('ImportingDataBankModule');
			  }] }
		  })
		  .state('app.listPay', {
			url: 'List-payment',
			component: 'listPay',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('ListPaymentModule');
				}] }
		})
		.state('app.password', {
			url: 'reset',
			component: 'password',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('PasswordModule');
				}] },
			data: {
				permissions: {
				only: 'superadmin',
				redirectTo: 'app.home'
				}
			}
		})
		.state('app.preload', {
			url: 'preload',
			component: 'preload',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('PreloadModule');
				}] }
		})
		.state('app.profile', {
			url: 'profile',
			component: 'profile',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('ProfileModule');
				}] }
		})
		.state('app.register-pay', {
			url: 'register-pay',
			component: 'registerPay',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('RegisterPayModule');
				}] }
		 })
		.state('app.bc', {
			url: 'report/today',
			component: 'bc',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('TodayModule');
				}] }
			})
		.state('app.dg', {
			url: 'report/filter',
			component: 'md',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('TotalModule');
				}] }
		 })	
		 .state('app.search', {
			url: 'search',
			component: 'search',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('SearchModule');
				}] }
		})
		.state('app.searchadmin', {
			url: 'search-admin',
			component: 'searchadmin',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('SearchAdminModule');
				}] },
			data: {
				permissions: {
					only: 'superadmin',
					redirectTo: 'app.home'
				}
			}
		})
		.state('app.tc', {
			url: 'delete-transactions',
			component: 'tc',
			resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  
					return $ocLazyLoad.load('TransactionCompanyModule');
				}] },
			data: {
				permissions: {
					only: 'eliminar',
					redirectTo: 'app.home'
				}
			}
		})
		.state('app.consulting', {
            url: 'security/consulting',
            component: 'consulting',
            resolve: { 
                redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('ConsultingModule');
                }]
            },
            data: {
                permissions: {
                    only: 'seguridad.auditoria',
					redirectTo: 'app.home'
                }
            }
        })
        .state('app.permission', {
            url: 'security/permission',
            component: 'permissions',
            resolve: { 
                redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('PermissionsModule');
                }]
            },
            data: {
                permissions: {
                    only: 'seguridad.permisos',
                    redirectTo: 'app.home'
                }
            }
        })
        .state('app.roles', {
            url: 'security/roles',
            component: 'roles',
            resolve: { 
                redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('RolesModule');
                }]
            },
            data: {
                permissions: {
                    only: 'seguridad.roles',
                    redirectTo: 'app.home'
                }
            }
        })
        .state('app.users', {
            url: 'security/users',
            component: 'users',
            resolve: { 
                redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('UsersModule');
                }] 
            },
            data: {
                permissions: {
                    only: 'seguridad.users',
                    redirectTo: 'app.home'
                }
            }
        })
        .state('app.usersDetails', {
            url: 'security/users/:id',
            component: 'usersDetails',
            resolve: { 
                redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('UsersDetailsModule');
                }] 
            },
            data: {
                permissions: {
                    only: ['seguridad.usuarios.details'],
                    redirectTo: 'app.home'
                }
            }
		});
	}

    function loadTransitions($transitions, $rootScope) {
        $transitions.onStart({}, function(transition) {
            $rootScope.loader = true;
        });

        $transitions.onSuccess({}, function(transition) {
       
     $rootScope.loader = false;
        });
    }
	stateRoutes.$inject = [ 
		'$stateProvider', 
		'$urlRouterProvider', 
		'$locationProvider' 
	];

    loadTransitions.$inject = [ 
        '$transitions', 
        '$rootScope'
    ];
})();