(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.listPay', {
                url: 'List-payment',
                component: 'listPay',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
            });
    }

    stateRoutes.$inject = [
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();