(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.profile', {
                url: 'profile',
                component: 'profile',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();