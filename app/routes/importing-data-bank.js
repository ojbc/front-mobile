(function(){
  'use strict';

  angular
      .module('app')
      .config(stateRoutes);

  /** @ngInject */
  function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
      .state('app.importing-data-bank', {
        url: 'importing-data-bank',
        component: 'idb',
        resolve: {
          redirectIfNotAuthenticated: _redirectIfNotAuthenticated
        }
      });
  }

  stateRoutes.$inject = [
      '$stateProvider',
      '$urlRouterProvider',
      '$locationProvider'
  ];

})();
