(function(){
    'use strict';

    angular
      .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('app.login', {
          url: 'login',
          component: 'auth',
          resolve: { skipIfAuthenticated: _skipIfAuthenticated }
        });
    }

    stateRoutes.$inject = [ 
        '$stateProvider', 
        '$urlRouterProvider', 
        '$locationProvider' 
    ];

})();