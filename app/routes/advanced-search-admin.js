(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.searchadmin', {
                url: 'search-admin',
                component: 'searchadmin',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
                data: {
                    permissions: {
                        only: 'superadmin',
                        redirectTo: 'app.home'
                    }
                }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();