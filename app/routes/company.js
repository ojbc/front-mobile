(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.admin', {
                url: 'superadmin',
                component: 'company',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
                data: {
                    permissions: {
                    only: 'superadmin',
                    redirectTo: '/home'
                    }
                }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();