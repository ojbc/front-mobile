(function(){
    'use strict';

    angular
      .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('app.register-pay', {
          url: 'register-pay',
          component: 'registerPay',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
        });
    }

    stateRoutes.$inject = [ '$stateProvider', '$urlRouterProvider', '$locationProvider' ];

})();