(function(){
    'use strict';

    angular
      .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('app.relation-pay', {
          url: 'relation-pay',
          component: 'relationPay',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
        });
    }

    stateRoutes.$inject = [ '$stateProvider', '$urlRouterProvider', '$locationProvider' ];

})();