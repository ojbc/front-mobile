(function(){
    'use strict';

    angular
      .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('app.roles', {
          url: 'security/roles',
          component: 'roles',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
          data: {
            permissions: {
              only: 'seguridad.roles',
              redirectTo: '/home'
            }
          }
        })
        .state('app.permission', {
          url: 'security/permission',
          component: 'permissions',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
          data: {
            permissions: {
              only: 'seguridad.permisos',
              redirectTo: '/home'
            }
          }
        })
        .state('app.users', {
          url: 'security/users',
          component: 'users',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
          data: {
            permissions: {
              only: 'seguridad.usuarios',
              redirectTo: '/home'
            }
          }
        })
        .state('app.usersDetails', {
          url: 'security/users/:id',
          component: 'usersDetails',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
          data: {
            permissions: {
              only: ['seguridad.usuarios'],
              redirectTo: '/home'
            }
          }
        })
        .state('app.consulting', {
          url: 'security/consulting',
          component: 'consulting',
          resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
          data: {
            permissions: {
              only: 'seguridad.auditoria',
              redirectTo: '/home'
            }
          }
        });
    }

    stateRoutes.$inject = [ 
        '$stateProvider', 
        '$urlRouterProvider', 
        '$locationProvider'
    ];

})();