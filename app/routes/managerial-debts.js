(function() {
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.dg', {
                url: 'report/filter',
                component: 'md',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
            }).state('app.bc', {
                url: 'report/today',
                component: 'bc',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
                });
    }

    stateRoutes.$inject = [
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();