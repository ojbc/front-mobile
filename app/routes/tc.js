(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.tc', {
                url: 'delete-transactions',
                component: 'tc',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
                data: {
                    permissions: {
                        only: 'eliminar',
                        redirectTo: 'app.home'
                    }
                }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();