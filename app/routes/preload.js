(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.preload', {
                url: 'preload',
                component: 'preload',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
            });
    }

    stateRoutes.$inject = [ '$stateProvider', '$urlRouterProvider', '$locationProvider' ];

})();