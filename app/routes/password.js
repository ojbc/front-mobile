(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.password', {
                url: 'reset',
                component: 'password',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
                data: {
                    permissions: {
                    only: 'superadmin',
                    redirectTo: '/home'
                    }
                }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();