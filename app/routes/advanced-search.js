(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.search', {
                url: 'search',
                component: 'search',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();