(function(){
    'use strict';

    angular
        .module('app')
        .config(stateRoutes);

    /** @ngInject */
    function stateRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('app.dt', {
                url: 'delete-transaction',
                component: 'dt',
                resolve: { redirectIfNotAuthenticated: _redirectIfNotAuthenticated },
                data: {
                    permissions: {
                        only: 'superadmin',
                        redirectTo: 'app.home'
                    }
                }
            });
    }

    stateRoutes.$Inject = [

        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider'
    ];

})();