(function() {
  'use strict';

  /** @ngInject */
  function restService($resource, ApiUrl, $log, $q, notification) {
    var vm = this;

    function isVerifiedEndPoint() {
      if (angular.isDefined(ApiUrl) || ApiUrl !== '') {
        return true;
      }

      $log.error('Site Required Url Api.');
      return false;
    }

    function getService(endpoint, attrs) {
      if (isVerifiedEndPoint()) {
        return $resource(ApiUrl + endpoint, {}, {
          'query': {
            method: 'GET',
            isArray: false,
            params: attrs
          }
        });
      }
    }

    function postService(endpoint, attrs) {
      if (isVerifiedEndPoint()) {
        return $resource(ApiUrl + endpoint).save(attrs);
      }
    }

    function executeCallService(attrsObject) {
      var q = $q.defer();

      switch (attrsObject.method) {
        case 'GET':
          var query = {
            query: {
              url: ApiUrl + attrsObject.endpoint,
              method: 'GET',
              isArray: false,
              params: angular.isDefined(attrsObject.attrs) ? attrsObject.attrs : {}
            }
          };

          $resource(ApiUrl + attrsObject.endpoint, angular.isDefined(attrsObject.query) ? attrsObject.query : {}, query)
            .query(attrsObject.attrs)
            .$promise.then(function (response) {
              if (response.status === 200) {
                q.resolve(response.data);
              } else {
                q.reject(response);
                if (attrsObject.alert) {
                  notification.error.resources(response);
                }
              }
            });

          return q.promise;

        case 'POST':
          $resource(ApiUrl + attrsObject.endpoint, angular.isDefined(attrsObject.query) ? attrsObject.query : {})
            .save(attrsObject.attrs)
            .$promise.then(function (response) {
              if (response.status === 200) {
                if (attrsObject.alert) {
                  notification.alert.show('Operación exitosa', 'Su operación ha sido realizada exitosamente', 'success');
                }
                q.resolve(response.data);
              } else {
                q.reject(response);
                notification.error.resources(response);
              }
            });

          return q.promise;
        default:
          //
          break;
      }
    }

    vm.callServ = {
      get: getService,
      post: postService,
      verified: isVerifiedEndPoint,
      executeCallService: executeCallService
    };
  }

  restService.$inject = ['$resource', 'ApiUrl', '$log', '$q', 'notification'];

  angular
    .module('app')
    .service('restService', restService);

})();