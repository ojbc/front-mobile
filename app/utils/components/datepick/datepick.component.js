(function() {
  'use strict';

  angular
    .module('app')
    .component('datepick', {
      templateUrl: 'app/utils/components/datepick/datepick.html',
      controller: ['$log', function($log) {
        var $ctrl = this;

        $ctrl.$onInit = activate;

        function activate() {
          $log.info('Date Picker Loaded');

          $ctrl.formats = ['dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $ctrl.format = $ctrl.formats[0];

          $ctrl.opened = false;
        }

        $ctrl.open = function () {
          $ctrl.opened = true;
        };
      }],
      bindings: {
        datepickobj: '=',
        namedatepicker: '@'
      }
    });
})();