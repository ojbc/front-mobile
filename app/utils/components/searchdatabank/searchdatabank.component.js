(function () {
  'use strict';

  angular
  .module('app')
  .component('searchDataBank', {
    templateUrl: 'app/utils/components/searchdatabank/searchdatabank.html',
    controller: ['$log', 'restService', function ($log, restService) {
      var $ctrl = this;

      $ctrl.$onInit = activate;
      $ctrl.dataregList = [];

      function activate() {
        $log.info('Loaded Search User Component');
      }

      $ctrl.refresRef = function (reference) {
        return restService.callServ.executeCallService({
          endpoint: '/importing-data-bank/get-transaction-log/',
          method: 'GET',
          attrs: {
            reference: reference
          },
          query: {},
          alert: false
        }).then(function (response) {
          $ctrl.dataregList = response.transaction;
        }).then(function () {
          //
        });
      };
    }],
    bindings: {
      datareg: '=',
      eventOnSelect: '&'
    }
  });

})();
