(function () {
  'use strict';

  function notification(SweetAlert, $q, system) {
    var vm = this;

    

    vm.error = {
      resources: function (response) {
        switch (response.error_code) {
          case 'internal_server_error':

            system.notify.show({
              title: 'Oops!',
              text: 'Estamos presentando fallas con el servidor',
              class_name: 'color youtube',
              time: 2000
            });

            break;
          case 'no_record_found':

            system.notify.show({
              title: 'Oops!',
              text: 'No se encontraron registros',
              class_name: 'color yellow',
              time: 2000
            });

            break;
          case 'is_not_registered':

            system.notify.show({
              title: 'Oops!',
              text: response.error_index + ', no está registrado',
              class_name: 'color yellow',
              time: 2000
            });

            break;
          case 'invalid_fields':
            //
            break;
          case 'custom_error_message':

            system.notify.show({
              title: 'Oops!',
              text: response.error_message,
              class_name: 'color yellow',
              time: 2000
            });

            break;
          default:
            vm.alert.show('Información!', 'Error de respuesta no registrada en nuestro servidor', 'error');
            break;
        }
      }
    };

    vm.alert = {
      show: function (title, message, type) {
        return SweetAlert.swal(title, message, type);
      },
      confirm: function (options) {
        var q = $q.defer();

        var opt = {
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Si, estoy de acuerdo!',
          cancelButtonText: 'No, cancelar'
        };

        SweetAlert.swal({
          title: options.title,
          text: options.message,
          type: angular.isDefined(options.type) ? options.type : 'success',
          showCancelButton: true,
          confirmButtonColor: opt.confirmButtonColor,
          confirmButtonText: opt.confirmButtonText,
          cancelButtonText: opt.cancelButtonText,
          closeOnConfirm: false,
          closeOnCancel: false
        }, function (isConfirm) {
          if (isConfirm) {
            q.resolve();
          } else {
            q.reject();
          }
        });

        return q.promise;
      }
    };
  }

  notification.$inject = ['SweetAlert', '$q', 'system'];

  angular
    .module('app')
    .service('notification', notification);
})();
