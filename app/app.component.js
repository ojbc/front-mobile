(function(){
    'use strict';

    angular
      .module('app')
      .component('app', {
        templateUrl: 'app/modules/template/template.html',
        controller: 'appController',
        controllerAs: 'vm'
      });

})();