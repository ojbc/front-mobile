angular
  .module('app')
  .component('searchUser', {
    templateUrl: 'app/components/searchuser/searchuser.component.html',
    controller: ['$log', 'restService', function ($log, restService) {
      var $ctrl = this;

      $ctrl.$onInit = activate;
      $ctrl.userList = [];

      function activate() {
        
      }

      $ctrl.refreshUser = function (name) {
        var params = {};

        if (angular.isDefined($ctrl.opts.role)) {
          switch ($ctrl.opts.role) {
            case 'MANAGER':
              params = {
                email: name,
                role: 3
              }; 
              break;
          }
        } else {
          params = {
            email: name
          };
        }

        return restService.callServ.executeCallService({
          endpoint: '/users/find-users',
          method: 'POST',
          attrs: params,
          query: {},
          alert: false
        }).then(function (response) {
          $ctrl.userList = response.users;
        }).then(function () {
          //
        });
      };
    }],
    bindings: {
      userobj: '=',
      opts: '=',
      eventOnSelect: '&'
    }
  });
