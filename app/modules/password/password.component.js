(function(){
    'use strict';

    angular
        .module('app')
        .component('password', {
            templateUrl: 'app/modules/password/password.html',
            controller: 'passwordController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();