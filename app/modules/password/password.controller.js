(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo contraseña.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function passwordController(system, securityResource, SweetAlert, ModalService, $state, localStorageService, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // DEFAULTS
        vm.userId = localStorageService.get('auth_userid');
        vm.selected = true;
        vm.loading = false;
        if(typeof PermPermissionStore.getStore().superadmin  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        vm.option= true;
        /*------------------------------------end---------------------------------------*/

        /*--------------------------------------Function_GET-------------------------------------*/
       
        vm.clearUser=function(){
            vm.form = {
             
            };
            vm.selected = true;
            vm.option= true;
            vm.form={
                company:{name:''},
               
                profile:{
                   
                    full_name: '',
                    name: ''
                },
                role:{
                    name:'',
                },
                passwordupdate: '',

            },
            vm.option= true;
            
         }
         
         vm.activ =function(){
            vm.selected = true;
         }
         vm.getCompany = function() {
            vm.loading_company = true;

            securityResource.users.allUserCompany.query()
                .$promise.then(function(response) {
                                
                if(response.status === 200) {
                    vm.listCompany = response.data;
                    vm.loading_company = false;
                }
            });
        }
        vm.selectCompany = function(exc) {
            vm.selected = false;
            vm.option= false;
            vm.form = angular.copy(exc);
           }
      
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/



        vm.save = function() {
            vm.loading = true;
            securityResource.users.resetPassword({
                     id:vm.form.id,
                     password: vm.form.passwordupdate
                })
                .$promise.then(function(response) {
                vm.loading = false;
                if (response.status === 200) {
                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });
                    vm.getCompany();
                    vm.clearUser();
                   
                }else{
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives,
                        type: "error",
                    });
                }
            }, function(response) {
                vm.loading = false;
                if (response.status === 401) {
                  
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives,
                        type: "error",
                    });
                    vm.activ();
                   
                }
                
                system.managerErrors(response);
               
                vm.activ();
            });
        }
       

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
       
        vm.getCompany();
        /*----------------------------------------end--------------------------------------------*/
    }

    passwordController.$inject = [
        'system',
        'securityResource',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        '$location', 
        'PermPermissionStore'

    ];

    angular
        .module('app')
        .controller('passwordController', passwordController);

})();