(function(){
    'use strict';

    angular
        .module('app')
        .component('tc', {
            templateUrl: 'app/modules/transaction-company/TC.html',
            controller: 'TCController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();