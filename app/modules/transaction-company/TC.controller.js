(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo company.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function TCController(system, SweetAlert, ModalService, $state, localStorageService, bankResource, $location, PermPermissionStore, ImportingDataResource) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // DEFAULTS
        vm.d = new Date();
        vm.loading_register =false;
        vm.hour = vm.d.toLocaleTimeString();
        vm.dd = vm.d.getDate();
        vm.mm = vm.d.getMonth()+1;
        vm.yyyy = vm.d.getFullYear();
        vm.userId = localStorageService.get('auth_userid');
        vm.companyId = localStorageService.get('auth_companyid');
        vm.loading = false;
        vm.form = {
            bankselect: '',
            companyselect:''
        }
        vm.selected =true;
        vm.id= '';
        vm.form = {
            cuota1: {
                opened: false,

            },
            cuota2: {
                opened: false,

            }

        };

        vm.dateOption1 = {
            maxDate: new Date(),
            showWeeks: false,
            minDate: new Date(1900, 1, 1),
        };

        vm.dateOption2 = {
            maxDate: new Date(),
            showWeeks: false,
            minDate: new Date(1900, 1, 1),
        };
     if(typeof PermPermissionStore.getStore().eliminar  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/

        /*--------------------------------------Function_GET-------------------------------------*/
        vm.changeMinDate = function(obj, minDate) {
            obj.minDate = minDate;
        }
        vm.d = new Date();


        vm.calendarOpen = function(calendar) {
            //
            calendar.opened = true;
        }

        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        vm.formatDate = function(){
            vm.d = new Date(vm.form.cuota1.date);
            vm.dd = vm.addZero(vm.d.getDate());
            vm.mm = vm.addZero(vm.d.getMonth()+1);
            vm.yyyy = vm.addZero(vm.d.getFullYear());
            vm.form.date = vm.yyyy+'-'+vm.mm+'-'+ vm.dd;
        }
        vm.formatDate2 = function(){
            vm.d = new Date(vm.form.cuota2.date);
            vm.dd = vm.addZero(vm.d.getDate());
            vm.mm = vm.addZero(vm.d.getMonth()+1);
            vm.yyyy = vm.addZero(vm.d.getFullYear());

            vm.form.dates = vm.dd+'/'+vm.mm+'/'+vm.yyyy;
        }
        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        vm.fullHour = function() {
            var h = vm.addZero(vm.d.getHours());
            var m = vm.addZero(vm.d.getMinutes());
            var s = vm.addZero(vm.d.getSeconds());
            vm.hour = h + ":" + m + ":" + s;
        }
        vm.fullDate = function() {
            var dd = vm.addZero(vm.d.getDate());
            var mm = vm.addZero(vm.d.getMonth()+1);
            var yyyy = vm.addZero(vm.d.getFullYear());
            vm.date = dd + ":" + mm + ":" + yyyy;
        }



        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.all.query({
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {

                    if (response.status === 200) {
                        vm.listBanks = response.data;
                        vm.loading_banks = false;
                    }
                });
        }


        vm.selectAssociation = function(exc) {
                   
            vm.form.bankselect = exc.banks;
            vm.id =exc.id;
            vm.selected = false;
            
        }
        vm.clearCompany = function(){
            vm.form.bankselect= "";
            vm.form.companyselect= "";
            vm.selected = true;
            vm.getBanks();

            vm.form = {
                cuota1: {
                    opened: false,

                },
                cuota2: {
                    opened: false,

                },
            }
            vm.dateOption1 = {
                maxDate: new Date(),
                showWeeks: false,
                minDate: new Date(1900, 1, 1),
            };

            vm.dateOption2 = {
                maxDate: new Date(),
                showWeeks: false,
                minDate: new Date(1900, 1, 1),
            };
        }
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.deleteTransaction = function(data)
        {
            vm.form.cuota2.date = data.cuota2.date;
            if(vm.form.cuota2.date){
                vm.formatDate2();
            }

            SweetAlert.swal({
                title: "Desea Eliminarla?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },function(isConfirm){
                if (isConfirm) {
                    vm.loading = true;
                    ImportingDataResource.deleteTransaction(
                        {
                            today: vm.form.dates,
                            bank:vm.form.bankselect.banks.name,
                            company:vm.companyId

                        }
                    )
                        .$promise.then(function(response) {
                        vm.loading = false;
                        if (response.status === 200) {
                            SweetAlert.swal({
                                title: "Eliminado!",
                                text: 'Operación realizada correctamente!',
                                type: "success",
                            });
                            vm.clearCompany();
                        }else{
                            system.managerErrors(response);
                            vm.clearCompany();
                        }
                    }, function(response) {
                        vm.loading = false;
                        SweetAlert.swal({
                            title: "Oops!",
                            text: response.data.motives + '. Por favor intenta nuevamente!',
                            type: "error",
                        });
                        vm.clearCompany();
                    });
                }
            });

        }


        
        

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getBanks();

        /*----------------------------------------end--------------------------------------------*/
    }

    TCController.$inject = [
        'system',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        'bankResource',
        '$location', 
        'PermPermissionStore',
        'ImportingDataResource'
    ];

    angular
        .module('app')
        .controller('TCController', TCController);

})();