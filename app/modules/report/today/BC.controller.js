(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Colecciones Facturadas.                     |
    ------------------------------------end-------------------------------------*/

    /** @ngInject */
    function BCController(system, SweetAlert, localStorageService, registerPayResource, $location, PermPermissionStore, ApiUrl, $auth, services, $scope) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.f = new Date();

        vm.companyId = localStorageService.get('auth_companyid');
        vm.d = new Date();
        vm.printfB = false;
        vm.pdf = [];
        vm.listCDN = 0.0;
        if(typeof PermPermissionStore.getStore().reporte  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/

       
        //Hora ---------------------------
        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
        }

        vm.fullHour = function() {
            var h = vm.addZero(vm.d.getHours());
            var m = vm.addZero(vm.d.getMinutes());
            var s = vm.addZero(vm.d.getSeconds());
            vm.hour = h + ":" + m + ":" + s;
        }
        vm.fullHour();
        //--------------------------------
        //Fechas -------------------------
        vm.hour = vm.d.toLocaleTimeString();
        vm.dd = vm.d.getDate();
        vm.mm = vm.d.getMonth() + 1;
        vm.yyyy = vm.d.getFullYear();
        //--------------------------------

        if (vm.dd < 10) {
            vm.dd = '0' + vm.dd;
        }

        if (vm.mm < 10) {
            vm.mm = '0' + vm.mm;
        }
        vm.time = vm.dd + '/' + vm.mm + '/' + vm.yyyy;
        vm.day = vm.f.getDate();
        vm.month=(vm.f.getMonth() +1);
        if(vm.day <= 10){
            vm.days = ("0"+ vm.day);
        } else {
            vm.days =  vm.day;
        }
        if(vm.month <= 10){
            vm.months = ("0"+ vm.month);

        }else{
            vm.months =  vm.month;
        }
        vm.t = vm.f.getFullYear() + "-" +  vm.months + "-" +  vm.days;

        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/


        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/

        vm.searchBilled = function() {
            vm.loading = true;
            vm.listCDN = 0.0;
            registerPayResource.today({
                data:  vm.t,
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.listCDN = response.data[0].suma;
                        vm.loading = false;
                        
                    }
                }, function(response) {
                   
                    vm.loading = false;
                });
        }

      

            
    
            vm.dtOptions = {
                scrollX: true,
                destroy: true,
                cache: false,
                processing: true,
                serverSide: true,
                sorting: [[0, 'asc']],
                ajax: {
                    url: ApiUrl + services.registerPay.todaysPaginates,
                    type: "GET",
                    data: function(d) {
                            d.data =vm.t;
                            d.company_id =vm.companyId;
                    },
                    headers: {
                        'Authorization': 'Bearer ' + $auth.getToken()
                    }
                },
                columns: [
                   
                    { data: "id" },
                    { data: "full_name" },
                    { data: "reference" },
                    { data: "bank" },
                    { data: "created_at" },
                    { data: "amount" },
                    { data: "description" },
                ],
                language: {
                    sLengthMenu:     "Mostrar  _MENU_",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "Ningún dato disponible en esta tabla",
                    sInfo:           "Del _START_ al _END_",
                    sInfoEmpty:      "0 Registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar: ",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "Cargando...",
                    sProcessing: "Cargando",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },  
            };
    
            vm.dtManager = $('#table').DataTable(vm.dtOptions);
    
    
            vm.print = function(type) {
               
                vm.loading = true;
    
                registerPayResource.printPdfToday({
                    data:  vm.t,
                    company_id: vm.companyId,
                    type: type
                }).$promise.then(function(response) {
                    vm.loading = false;
                    if(response.status === 200) {
                    
                        if (response.data.url)
                            window.open(response.data.url, '_blank');
                    }
                }, function(response) {
                    system.managerErrors(response);
                    vm.loading = false;
                });
            }
        
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/

        vm.searchBilled();

        /*----------------------------------------end--------------------------------------------*/
    }

    BCController.$inject = [
        'system',
        'SweetAlert',
        'localStorageService',
        'registerPayResource',
        '$location', 
        'PermPermissionStore',
        'ApiUrl', 
        '$auth', 
        'services', 
        '$scope'
    ];

    angular
        .module('app')
        .controller('BCController', BCController);

})();