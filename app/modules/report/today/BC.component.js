(function() {
    'use strict';

    angular
        .module('app')
        .component('bc', {
            templateUrl: 'app/modules/report/today/BC.html',
            controller: 'BCController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();