(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Reporte total el cual se                    |
    | encarga verificar como registrar todos los pagos                          |
    ------------------------------------end-------------------------------------*/

    /** @ngInject */
    function MDController(system, SweetAlert, securityResource, localStorageService, analystsResource, registerPayResource, $location, PermPermissionStore, ApiUrl, $auth, services, $scope) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.f = new Date();
        vm.companyId = localStorageService.get('auth_companyid');
        vm.t = vm.f.getDate() + "/" + (vm.f.getMonth() +1) + "/" + vm.f.getFullYear();
        vm.d = new Date();
        vm.selectedType = false;
        vm.analyst_select = false;
        vm.printfB = false;
        vm.loading = false;
        vm.listCDN = 0.0;
        if(typeof PermPermissionStore.getStore().reporte  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        vm.form = {
            cuota1: {
                opened: false,

            },
            cuota2: {
                opened: false,

            },
            cuota3: {
                opened: false,

            },
            cuota4: {
                opened: false,

            }
        };

        vm.dateOption1 = {
            maxDate: new Date(),
            showWeeks: false,
            minDate: new Date(1900, 1, 1),
        };

        vm.dateOption2 = {
            maxDate: new Date(),
            showWeeks: false,
            minDate: new Date(1900, 1, 1),
        };
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/

        vm.pdf = [];
        vm.changeMinDate = function(obj, minDate) {
            obj.minDate = minDate;
        }
        vm.d = new Date();


        vm.calendarOpen = function(calendar) {
            //
            calendar.opened = true;
        }
        vm.clearInputs = function(form) {
            vm.form = {};
            form.$setPristine();
            vm.form = {
                analysts : '',
                cuota1: {
                    opened: false,

                },
                cuota2: {
                    opened: false,

                },
                
            }
            vm.listCDN = 0.0;
            vm.selectedScale = false;
            vm.dateOption1 = {
                maxDate: new Date(),
                showWeeks: false,
                minDate: new Date(1900, 1, 1),
            };

            vm.dateOption2 = {
                maxDate: new Date(),
                showWeeks: false,
                minDate: new Date(1900, 1, 1),
            };
            vm.getAnalyst();
            vm.dtManager = $('#table').DataTable(vm.dtOptions);
           
        }


        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        vm.formatDate = function(){
            vm.d = new Date(vm.form.cuota1.date);
            vm.dd = vm.addZero(vm.d.getDate());
            vm.mm = vm.addZero(vm.d.getMonth()+1);
            vm.yyyy = vm.addZero(vm.d.getFullYear());
            vm.form.date = vm.yyyy+'-'+vm.mm+'-'+ vm.dd;
        }
        vm.formatDate2 = function(){
            vm.d = new Date(vm.form.cuota2.date);
            vm.dd = vm.addZero(vm.d.getDate());
            vm.mm = vm.addZero(vm.d.getMonth()+1);
            vm.yyyy = vm.addZero(vm.d.getFullYear());
            vm.form.dates = vm.yyyy +'-'+vm.mm+'-'+  vm.dd;
        }
        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        vm.fullHour = function() {
            var h = vm.addZero(vm.d.getHours());
            var m = vm.addZero(vm.d.getMinutes());
            var s = vm.addZero(vm.d.getSeconds());
            vm.hour = h + ":" + m + ":" + s;
        }
        vm.fullDate = function() {
            var dd = vm.addZero(vm.d.getDate());
            var mm = vm.addZero(vm.d.getMonth()+1);
            var yyyy = vm.addZero(vm.d.getFullYear());
            vm.date = dd + ":" + mm + ":" + yyyy;
        }
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/

        vm.getUsers = function() {
            vm.loading_users = true;
            securityResource.users.all.query({ 
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.userstxt = response.data;
                        vm.form.users = vm.userstxt[0];
                    }
                });
        }

        vm.getAnalyst = function() {
            analystsResource.all.query({ 
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.listanalyst = response.data;
                        vm.form.analyst = vm.listanalyst;
                      
                    }
                });
        }

        
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        //Busqueda de pagos fecha y analista.
        $scope.tempHtml = '';
        vm.dtOptions = {
    
            scrollX: true,
            destroy: true,
            cache: false,
            processing: true,
            serverSide: true,
            sorting: [[0, 'desc']],
            ajax: {
                url: ApiUrl + services.registerPay.totalPaginates,
                type: "GET",
                data: function(d) {
                    d.company_id=vm.companyId;  
                    if(vm.form){
                        if(vm.form.analysts){
                          
                            d.analyst=vm.form.analysts.id;
                        }
                        if(vm.form.cuota2.date){
                            vm.formatDate2();
                           
                            
                            d.d1= vm.form.dates;
                        }
                        if(vm.form.cuota1.date){
                            vm.formatDate();
                            
                            
                             d.d2=vm.form.date;
                        }
                    }
                    
                     
                },
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                }
                
            },
            columns: [
                { data: "id" },
                { data: "full_name" },
                { data: "reference" },
                { data: "bank" },
                { data: "created_at" },
                { data: "amount" },
                { data: "description" },
            
        ],
            language: {
                sLengthMenu:     "Mostrar  _MENU_",
                sZeroRecords:    "No se encontraron resultados",
                sEmptyTable:     "Ningún dato disponible en esta tabla",
                sInfo:           "Del _START_ al _END_",
                sInfoEmpty:      "0 Registros",
                sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                sInfoPostFix:    "",
                sSearch:         "Buscar: ",
                sUrl:            "",
                sInfoThousands:  ",",
                sLoadingRecords: "Cargando...",
                sProcessing: "Cargando",
                oPaginate: {
                    sFirst:    "Primero",
                    sLast:     "Último",
                    sNext:     "Siguiente",
                    sPrevious: "Anterior"
                },
                oAria: {
                    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
        };
        vm.dtManager = $('#table').DataTable(vm.dtOptions);
        vm.searchBilled = function(data) {
           
           
                    vm.search(data);

            }

           vm.search= function(data) {
            vm.form.cuota1.date = data.cuota1.date;
            vm.form.cuota2.date = data.cuota2.date;
            
            if(vm.form.cuota1.date){
                vm.formatDate();
            }
            if(vm.form.cuota2.date){
                vm.formatDate2();
            }
            
            vm.dtManager = $('#table').DataTable(vm.dtOptions);
            vm.loading = true;
                 vm.loading_register = true;
                registerPayResource.total({
                        dato: vm.form,
                        company_id:vm.companyId
                    })
                    .$promise.then(function(response) {
                    vm.loading = false;
                        if (response.status === 200) {
                            vm.listCDN = response.data[0].suma;
                            vm.loading_register = false;
                            if (response.data.length === 0) {
                                SweetAlert.swal({
                                    title: "No hay pagos relacionadas.!",
                                    type: "warning",
                                });
                            }
                        }
                    }, function(response) {
                    vm.loading = false;
                        SweetAlert.swal({
                            title: "Oops!",
                            text: response.data.motives + '. Por favor intenta nuevamente!',
                            type: "error",
                        });
                    });
           }

       
            
    
            
            //Generar el PDF de las deudas del gerente.
            vm.print = function(type) {
               
                vm.loading = true;
                if(vm.form.cuota1.date){
                    vm.formatDate();
                }
                if(vm.form.cuota2.date){
                    vm.formatDate2();
                }
                registerPayResource.printPdfMD({
                    dato: vm.form,
                    data:  vm.t,
                    company_id: vm.companyId,
                    type: type
                }).$promise.then(function(response) {
                    vm.loading = false;
                    if(response.status === 200) {
                      
                        if (response.data.url)
                            window.open(response.data.url, '_blank');
                    }
                }, function(response) {
                    system.managerErrors(response);
                    vm.loading = false;
                });
            }

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getUsers();
        vm.getAnalyst();
        vm.fullHour();
        vm.fullDate();
        /*----------------------------------------end--------------------------------------------*/
    }


    MDController.$inject = [  
        'system', 
        'SweetAlert', 
        'securityResource', 
        'localStorageService', 
        'analystsResource', 
        'registerPayResource', 
        '$location', 
        'PermPermissionStore', 
        'ApiUrl', 
        '$auth', 
        'services', 
        '$scope'
    ];

    angular
        .module('app')
        .controller('MDController', MDController);

})();