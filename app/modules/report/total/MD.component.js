(function() {
    'use strict';

    angular
        .module('app')
        .component('md', {
            templateUrl: 'app/modules/report/total/MD.html',
            controller: 'MDController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();