(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
	|   Esta función tiene como finalidad realizar los métodos de               |
	| index del módulo home.                                                    |
	------------------------------------end-------------------------------------*/
    /** @ngInject */
	function homeController( system ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        /*------------------------------------end---------------------------------------*/
	}

	homeController.$inject = [
	   'system'
	];

	angular
	   .module('app')
	   .controller('homeController', homeController);

})();