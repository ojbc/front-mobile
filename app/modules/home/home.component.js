(function(){
    'use strict';

    angular
      .module('app')
      .component('home', {
        templateUrl: 'app/modules/home/home.html',
        controller: 'homeController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();