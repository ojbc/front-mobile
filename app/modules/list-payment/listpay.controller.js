(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta pagos registrados por el analista.                               |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function listpayController( system, SweetAlert, ModalService, $state, localStorageService, $location, PermPermissionStore, ApiUrl, services, $auth, $scope) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.loading_analyst = true;
        vm.companyId = localStorageService.get('auth_companyid');
        if(typeof PermPermissionStore.getStore().listas  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/


        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/


        $scope.tempHtml = '';

       
        vm.dtOptions = {

            scrollX: true,
            destroy: true,
            cache: false,
            processing: true,
            serverSide: true,
            sorting: [[0, 'desc']],
            ajax: {
                url: ApiUrl + services.registerPay.paginate,
                type: "GET",
                data: function(d) {
                    d.company_id= vm.companyId;
                  
                },
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                }
                
            },
            columns: [
                {data: "full_name"},
                { data: "reference" },
                { data: "bank" },
                { data: "voucher_date" },
                { data: "amount" },
                { data: "created_at" },
                { data: "description" },
            
        ],
            language: {
                sLengthMenu:     "Mostrar  _MENU_",
                sZeroRecords:    "No se encontraron resultados",
                sEmptyTable:     "Ningún dato disponible en esta tabla",
                sInfo:           "Del _START_ al _END_",
                sInfoEmpty:      "0 Registros",
                sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                sInfoPostFix:    "",
                sSearch:         "Buscar: ",
                sUrl:            "",
                sInfoThousands:  ",",
                sLoadingRecords: "Cargando...",
                sProcessing: "Cargando",
                oPaginate: {
                    sFirst:    "Primero",
                    sLast:     "Último",
                    sNext:     "Siguiente",
                    sPrevious: "Anterior"
                },
                oAria: {
                    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
        };

        vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        /*----------------------------------------end--------------------------------------------*/
    }

    listpayController.$inject = [
        'system',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        '$location', 
        'PermPermissionStore',
        'ApiUrl', 
        'services', 
        '$auth', 
        '$scope'
    ];

    angular
        .module('app')
        .controller('listpayController', listpayController);


})();