(function(){
    'use strict';

    angular
        .module('app')
        .component('listPay', {
            templateUrl: 'app/modules/list-payment/listpay.html',
            controller: 'listpayController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();