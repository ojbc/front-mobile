(function() {
    'use strict';

    angular
        .module('app')
        .component('preload', {
            templateUrl: 'app/modules/preload/preload.html',
            controller: 'preloadController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();