(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | carga previa mostrar e imprimir los datos registrados y no registrados.   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function findlistController( $element,  close,  preloadResource, SweetAlert, localStorageService) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.form = {};
        vm.f = new Date();
        vm.t = vm.f.getDate() + "/" + (vm.f.getMonth() +1) + "/" + vm.f.getFullYear();
        vm.d = new Date();
        vm.companyId = localStorageService.get('auth_companyid');
        vm.loading_register = false;
        vm.listAP=JSON.parse(localStorage.getItem('payment'));
        vm.listNP=JSON.parse(localStorage.getItem('paymentn'));
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/

        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        vm.fullHour = function() {
            var h = vm.addZero(vm.d.getHours());
            var m = vm.addZero(vm.d.getMinutes());
            var s = vm.addZero(vm.d.getSeconds());
            vm.hour = h + ":" + m + ":" + s;
        }
        vm.fullHour();
        vm.fullDate = function() {
            var dd = vm.addZero(vm.d.getDate());
            var mm = vm.addZero(vm.d.getMonth()+1);
            var yyyy = vm.addZero(vm.d.getFullYear());
            vm.date = dd + ":" + mm + ":" + yyyy;
        }

        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/



        
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/



            //Generar el PDF
            vm.printPdfnp = function(data) {

                vm.pdf = [data];
                vm.printfB = true;
                vm.loading_register = true;
                preloadResource.printPdfnp({
                    date: vm.t,
                    hour: vm.hour,
                    table: vm.pdf
                })
                    .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.loading_register = false;
                        if (response.data.url)
                        window.open(response.data.url, '_blank');


                    }
                }, function (response) {
                    vm.loading_register = false;
                    vm.motives = response.data.motives;
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives + '. Por favor intenta nuevamente!',
                        type: "error",
                    });

                });

            }
        vm.printPdfap = function(data) {
           
           // vm.loading_register = true;
           vm.pdf = [data];
            vm.printfB = true;

            preloadResource.printPdfap({
                date: vm.t,
                hour: vm.hour,
                table: vm.pdf

            }).$promise.then(function(response) {
                if (response.status === 200) {
                    vm.loading_register = false;
                    if(response.data.type === "download") {
                        var a = document.createElement('a');
                        a.href = response.data.url;
                        a.target = '_blank';
                        a.download = "";
                        a.click();
                    }


                }
            }, function (response) {
                vm.loading_register = false;
                vm.motives = response.data.motives;
                SweetAlert.swal({
                    title: "Oops!",
                    text: response.data.motives + '. Por favor intenta nuevamente!',
                    type: "error",
                });

            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/


        /*----------------------------------------end--------------------------------------------*/
    }

    findlistController.$inject = [
        '$element',
        'close',
        'preloadResource',
        'SweetAlert',
        'localStorageService'
    ];

    angular
        .module('app')
        .controller('findlistController', findlistController);

})();