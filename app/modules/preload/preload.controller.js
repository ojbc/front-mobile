(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo carga previa el cual se                    |
    | encarga verificar como registrar todos los pagos                          |
    ------------------------------------end-------------------------------------*/

    /** @ngInject */
    function preloadController(system, SweetAlert, ModalService, localStorageService, $location, PermPermissionStore, bankResource, preloadResource, preloadsResource, ApiUrl, services, $auth, $scope, $timeout, $compile) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.f = new Date();
        vm.companyId = localStorageService.get('auth_companyid');
        vm.userId = localStorageService.get('auth_userid');
        vm.form = {
            bankselect: ''
        }
        vm.t = vm.f.getDate() + "/" + (vm.f.getMonth() +1) + "/" + vm.f.getFullYear();
        vm.d = new Date();
        vm.selectedType = false;
        vm.printfB = false;
        vm.loading = false;

        if((typeof PermPermissionStore.getStore().carga !== 'undefined') || (typeof PermPermissionStore.getStore().cargas  !== 'undefined')){

        }else{
            $location.path('/home');
        }
        vm.form = {
            cuota1: {
                opened: false,

            },
            cuota2: {
                opened: false,

            },
            cuota3: {
                opened: false,

            },
            cuota4: {
                opened: false,

            }
        };

        vm.dateOption1 = {
            maxDate: new Date(),
            showWeeks: false,
            minDate: new Date(1900, 1, 1),
        };

        vm.dateOption2 = {
            maxDate: new Date(),
            showWeeks: false,
            minDate: new Date(1900, 1, 1),
        };
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/

        vm.pdf = [];
        vm.changeMinDate = function(obj, minDate) {
            obj.minDate = minDate;
        }
        vm.d = new Date();


        vm.calendarOpen = function(calendar) {
            //
            calendar.opened = true;
        }

        vm.getClear = function() {
                vm.form.reference = "";
                vm.form.amount = "";
                vm.form.description = "";
                vm.form.description = "";
                vm.form.bankselect="";
            vm.getBanks();
                vm.selectedScale = false;
            vm.form = {
                cuota2: {
                    opened: false,

                },
            }
            vm.dateOption1 = {
                maxDate: new Date(),
                showWeeks: false,
                minDate: new Date(1900, 1, 1),
            };

            vm.dateOption2 = {
                maxDate: new Date(),
                showWeeks: false,
                minDate: new Date(1900, 1, 1),
            };
        }
        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        vm.formatDate = function(){
            vm.d = new Date(vm.form.cuota1.date);
            vm.dd = vm.addZero(vm.d.getDate());
            vm.mm = vm.addZero(vm.d.getMonth()+1);
            vm.yyyy = vm.addZero(vm.d.getFullYear());
            vm.form.date = vm.yyyy+'-'+vm.mm+'-'+ vm.dd;
        }
        vm.formatDate2 = function(){
            vm.d = new Date(vm.form.cuota2.date);
            vm.dd = vm.addZero(vm.d.getDate());
            vm.mm = vm.addZero(vm.d.getMonth()+1);
            vm.yyyy = vm.addZero(vm.d.getFullYear());
            vm.form.dates = vm.dd+'/'+vm.mm+'/'+vm.yyyy;
        }
        vm.addZero = function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        vm.fullHour = function() {
            var h = vm.addZero(vm.d.getHours());
            var m = vm.addZero(vm.d.getMinutes());
            var s = vm.addZero(vm.d.getSeconds());
            vm.hour = h + ":" + m + ":" + s;
        }
        vm.fullDate = function() {
            var dd = vm.addZero(vm.d.getDate());
            var mm = vm.addZero(vm.d.getMonth()+1);
            var yyyy = vm.addZero(vm.d.getFullYear());
            vm.date = dd + ":" + mm + ":" + yyyy;
        }
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/


        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.all.query({
                company_id: vm.companyId,
                user_id: vm.userId
            })
                .$promise.then(function(response) {

                if(response.status === 200) {
                    vm.listBanks = response.data;
                    vm.loading_banks = false;
                }
            });
        }
        vm.getPreload = function() {
            vm.loading_register = true;

            preloadsResource.all.query({
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listCDN = response.data;
                    vm.loading_register = false;
                }
            }, function(response) {

                vm.listCDN = "";
                vm.loading_register = false;
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/

        vm.savePreload = function(data) {
            vm.form.cuota2.date = data.cuota2.date;

            if(vm.form.cuota2.date){
                vm.formatDate2();
            }
            vm.loading = true;
                 vm.loading_register = true;
                    preloadResource.save({
                        user_id: vm.userId,
                        reference:vm.form.reference,
                        amount:vm.form.amount,
                        description:vm.form.description,
                        bank:vm.form.bankselect.banks.name,
                        voucher_date: vm.form.dates,
                        company_id:vm.companyId
                    })
                    .$promise.then(function(response) {
                        vm.loading = false;
                        if (response.status === 200) {
                            SweetAlert.swal({
                                title: "Excelente!",
                                text: 'Operación realizada correctamente!',
                                type: "success",
                            });
                            vm.getClear();
                            vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
                            vm.loading_register = false;

                        }
                    }, function(response) {
                        vm.loading = false;
                        SweetAlert.swal({
                            title: "Oops!",
                            text: response.data.motives + '. Por favor intenta nuevamente!',
                            type: "error",
                        });
                    });
            }
        vm.deletePreload = function() {
            SweetAlert.swal({
                title: "Desea Eliminarla?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },function(isConfirm){
                if (isConfirm) {
          
            vm.loading_register = true;
            preloadResource.delete({
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                if (response.status === 200) {
                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });

                    vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
                    vm.loading_register = false;

                }
            }, function(response) {
                SweetAlert.swal({
                    title: "Oops!",
                    text: response.data.motives + '. Por favor intenta nuevamente!',
                    type: "error",
                });
                vm.loading_register = false;
            });
                }
            });
        }
        vm.checkPreload = function() {

            vm.loading_register = true;
            preloadResource.checkPreload({
                company_id: vm.companyId
            })
                .$promise.then(function(response) {

                if (response.status === 200) {
                    localStorage.setItem('paymentn', JSON.stringify(response.data.nprobados));
                    localStorage.setItem('payment', JSON.stringify(response.data.aprobados));
                    vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
                    vm.loading_register = false;
                    ModalService.showModal({
                        templateUrl: "findlist",
                        controller: "findlistController",
                        controllerAs: "vm",
                        inputs: {
                            transactionid: response.data
                        }
                    }).then(function(modal) {
                        modal.element.modal();

                    });



                }
            }, function(response) {
                SweetAlert.swal({
                    title: "Oops!",
                    text: response.data.motives + '. Por favor intenta nuevamente!',
                    type: "error",
                });
            });
        }

        vm.delete = function (data) {
            vm.id=data;
            SweetAlert.swal({
                title: "Desea Eliminarla?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                closeOnConfirm: false
            },function(isConfirm){
                if (isConfirm) {
                    vm.loading_register = true;
                    preloadResource.deletelist({ id: vm.id})
                        .$promise.then(function (response) {
                        if (response.status === 200) {
                            SweetAlert.swal({
                                title: "Excelente!",
                                text: 'Operación realizada correctamente!',
                                type: "success",
                            });

                            vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
                            vm.loading_register = false;
                        }
                    }, function (response) {
                        SweetAlert.swal({
                            title: "Oops!",
                            text: response.data.motives + '. Por favor intenta nuevamente!',
                            type: "error",
                        });
                        vm.loading_register = false;
                    });

                }
            });


        }
        $scope.tempHtml = '';

       
        vm.dtOptions = {

            scrollX: true,
            destroy: true,
            cache: false,
            processing: true,
            serverSide: true,
            sorting: [[0, 'desc']],
            ajax: {
                url: ApiUrl + services.preload.paginate,
                type: "GET",
                data: function(d) {
                    d.company_id= vm.companyId;
                  
                },
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                }
                
            },
            columns: [
                
                { data: "reference" },
                { data: "bank" },
                { data: "voucher_date" },
                { data: "amount" },
                { data: "description" },
                {data: "id"},
        ],
            language: {
                sLengthMenu:     "Mostrar  _MENU_",
                sZeroRecords:    "No se encontraron resultados",
                sEmptyTable:     "Ningún dato disponible en esta tabla",
                sInfo:           "Del _START_ al _END_",
                sInfoEmpty:      "0 Registros",
                sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                sInfoPostFix:    "",
                sSearch:         "Buscar: ",
                sUrl:            "",
                sInfoThousands:  ",",
                sLoadingRecords: "Cargando...",
                sProcessing: "Cargando",
                oPaginate: {
                    sFirst:    "Primero",
                    sLast:     "Último",
                    sNext:     "Siguiente",
                    sPrevious: "Anterior"
                },
                oAria: {
                    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            },
            drawCallback: function ( settings ) {

                $timeout(function() {
                    var table = angular.element('#dt_manager');
                    var rows = table.find('tr');
                    
                    rows.each(function(row) {
                        var element = angular.element(this);
                        var id = parseInt(element.find('td').eq(5).text());
                        $scope.tempHtml =  '<a href="#" class="icon" ng-click="vm.delete('+ id +')"' +'><i class="mdi mdi-delete"></i></a>';

                        var html = $compile($scope.tempHtml)($scope);
                        element.find('td').eq(5).html(html);
                    });
                }, 100);
            }  
        };

        vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getBanks()
        vm.fullHour();
        vm.fullDate();
        /*----------------------------------------end--------------------------------------------*/
    }


    preloadController.$inject = [
        'system',
        'SweetAlert',
        'ModalService',
        'localStorageService',
        '$location',
        'PermPermissionStore',
        'bankResource',
        'preloadResource',
        'preloadsResource', 
        'ApiUrl', 
        'services', 
        '$auth', 
        '$scope',
        '$timeout',
        '$compile'
    ];
    angular
        .module('app')
        .controller('preloadController', preloadController);

})();