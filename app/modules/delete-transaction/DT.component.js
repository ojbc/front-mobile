(function(){
    'use strict';

    angular
        .module('app')
        .component('dt', {
            templateUrl: 'app/modules/delete-transaction/DT.html',
            controller: 'DTController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();