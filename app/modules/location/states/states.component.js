(function(){
    'use strict';
    
    angular
      .module('app')
      .component('states', {
        templateUrl: 'app/modules/location/states/states.html',
        controller: 'stateController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();