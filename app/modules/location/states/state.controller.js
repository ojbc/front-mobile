(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Estados.                                    |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function stateController( system, locationResource ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/
        vm.getStates = function() {
            vm.loading_states = true;

            locationResource.states.all.query()
            .$promise.then(function(response) {
              if(response.status === 200) {
                vm.listStates = response.data;
                vm.loading_states = false;
              }
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getStates();
        /*----------------------------------------end--------------------------------------------*/
    }

    stateController.$inject = [
        'system',
        'locationResource'
    ];

    angular
        .module('app')
        .controller('stateController', stateController);

})();