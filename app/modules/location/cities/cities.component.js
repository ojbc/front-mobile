(function(){
    'use strict';

    angular
      .module('app')
      .component('cities', {
        templateUrl: 'app/modules/location/cities/cities.html',
        controller: 'citieController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();