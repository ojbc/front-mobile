(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Ciudades.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function citieController( system, locationResource ) {
        /*----------------------------------------Var--------------------------------------------*/
        var vm = this;

        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/
        vm.getCities = function() {
            vm.loading_cities = true;

            locationResource.cities.all.query()
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listCities = response.data;
                    vm.loading_cities = false;
                }
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getCities();
        /*----------------------------------------end--------------------------------------------*/
    }

    citieController.$inject = [
        'system',
        'locationResource'
    ];

    angular
        .module('app')
        .controller('citieController', citieController);

})();