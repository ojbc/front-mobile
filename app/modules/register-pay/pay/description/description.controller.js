(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | Registrar pago para incluir la descripcion.                                                           |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function finddescriptionController( $element,  close,  registerPayResource, SweetAlert, transactionid, ImportingDataResource, localStorageService) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.form = {};

        vm.companyId = localStorageService.get('auth_companyid');

        vm.transactionId  =localStorage.getItem('transactionid')
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
        vm.description = function() {
            ImportingDataResource.desData({
                id: vm.transactionId,
                description: vm.form.description
            })
                .$promise.then(function(response) {
                if (response.status === 200) {
                    $element.modal('hide');
                    close(response, 500);
                }
            }, function (response) {

                    vm.motives =  response.data.motives;
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives + '. Por favor intenta nuevamente!',
                        type: "error",
                    });
                });


        }


        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/



        
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/


        /*----------------------------------------end--------------------------------------------*/
    }

    finddescriptionController.$inject = [
        '$element',
        'close',
        'transactionid',
        'registerPayResource',
        'SweetAlert',
        'ImportingDataResource',
        'localStorageService'
    ];

    angular
        .module('app')
        .controller('finddescriptionController', finddescriptionController);

})();