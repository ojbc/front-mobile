(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | Registrar pago.                                                           |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function findPayModalController( $element,  close, bankResource, registerPayResource, SweetAlert, localStorageService) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.form = {};
        vm.selectedInvoice = false;
        vm.companyId = localStorageService.get('auth_companyid');
        vm.d = new Date();
        vm.hour = vm.d.toLocaleTimeString();
        vm.dd = vm.d.getDate();
        vm.mm = vm.d.getMonth()+1;
        vm.yyyy = vm.d.getFullYear();
      
    
        vm.form = {
            today: {
                opened: false,
            },
            transaction_date: {
                opened: true,
            },
        }
        vm.dateOption1 = {
            maxDate: new Date(),
            showWeeks: false
        }
        vm.today = {
          opened: false
        }
        
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
        vm.selectInvoice = function(invoice) {
            vm.today=vm.form.transaction_date.toLocaleDateString();
            vm.dts=vm.dateFormate(vm.today);
            //  Manually hide the modal using bootstrap.
            registerPayResource.check({
                data:  invoice,
                company_id: vm.companyId,
                bank: invoice.bankselect.banks.name,
                voucher_date: vm.dts
            })
                .$promise.then(function(response) {
                if (response.status === 200) {
                    if (response.data.length >= 1) {
                        $element.modal('hide');
                        close(response, 500);
                    }else{ SweetAlert.swal({
                        title: "Oops!",
                        text: 'No existe ningun pago con  esos datos' + '. Por favor intenta nuevamente!',
                        type: "error",
                    });
                        vm.clearInputsget();}
                }
            }, function (response) {

                    vm.motives =  response.data.motives;
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives + '. Por favor intenta nuevamente!',
                        type: "error",
                    });
                    vm.clearInputsget();
                });


        }
        vm.calendarOpen = function(test) {
            vm.today.opened = true;
        }

        vm.calendarOpen2 = function(calendar) {
            calendar.opened = false;
        }

        vm.returnValue =function(){
            vm.form.today ={ opened3: false };
        }
        vm.clearInputsget = function() {
            vm.form.amount="";
            vm.form.transaction_date="";
            vm.payments = "";
            vm.form = {
                today: {
                    opened: false,
                },
                transaction_date: {
                    opened: true,
                },
            },
            vm.dateOption1 = {
                maxDate: new Date(),
                showWeeks: false
            },
            vm.today = {
              opened: false
            }
        }
        vm.dateFormate = function(datetoday){
            vm.subcharacter = datetoday;
            vm.number=vm.subcharacter.length;
            if( vm.number < 12 ){
                if((vm.subcharacter[1]>=0) && (vm.subcharacter[2]=== '/') && (vm.subcharacter[5] != '/')){
                    vm.subCadenaFirst = vm.subcharacter[0];
                    vm.subCadenaSecond = vm.subcharacter[1];
                    vm.subCadenatree = vm.subcharacter[2];
                    vm.subCadenaLast = vm.subcharacter.substring(11,3)
                    vm.dateUpdate =  vm.subCadenaFirst + vm.subCadenaSecond + vm.subCadenatree + '0' + vm.subCadenaLast;

                    return(vm.dateUpdate);

                }else if((vm.subcharacter[1]==='/') && (vm.subcharacter[3]>=0)){
                    vm.dateUpdate = '0'+  vm.subcharacter;

                    return(vm.dateUpdate);

                }else if((vm.subcharacter[1]==='/') && (vm.subcharacter[3]==='/')){
                    vm.subCadena = vm.subcharacter[0];
                    vm.subCadenaLast = vm.subcharacter.substring(10,2)
                    vm.dateUpdate = '0'+ vm.subCadena + '/0' +vm.subCadenaLast;

                    return(vm.dateUpdate);

                } else{
                    vm.subcharacter;

                    return(vm.subcharacter);
                }
            }else{
                vm.caracter;

                return(vm.caracter);
            }
        }
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/
        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.all.query({
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                    
                if(response.status === 200) {
                    vm.listBanks = response.data;
                    vm.loading_banks = false;
                }
            });
        }
        


        
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        
        vm.save = function() {
            
            SweetAlert.swal({
                    title: "Registrar pago",
                    text: "¿Desea incluir el pago?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, deseo incluir!",
                    cancelButtonText: "No, cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {

                        registerPayResource.save({
                            invoice_id: vm.form.invoice.id,
                            user_id: vm.form.manager.id,
                            analyst_id: vm.userId,
                            form_pay: '0',
                            bank: vm.form.bankselect.banks,
                            reference: vm.form.ref,
                            voucher_date: vm.form.date,
                            amount: vm.form.amount,
                            company_id: vm.companyId


                        })
                            .$promise.then(function(response) {
                            if (response.status === 200) {
                                //console.log(response.data);
                                SweetAlert.swal({
                                    title: "Excelente!",
                                    text: 'Operación realizada correctamente!',
                                    type: "success",
                                });

                                vm.clearInputs();

                            }
                        }, function (response) {



                            vm.mostrar =  response.data;
                            vm.motives = vm.mostrar.motives;
                            if(vm.motives === "Este pago ya fue registrado"){

                                vm.isExistFacture();


                            }

                            SweetAlert.swal({
                                title: "Oops!",
                                text: response.data.motives + '. Por favor intenta nuevamente!',
                                type: "error",
                            });
                        });

                    } else {
                        SweetAlert.swal("Cancelado", "Tu pago no fue incluido", "error");
                    }
                });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getBanks();

        /*----------------------------------------end--------------------------------------------*/
    }

    findPayModalController.$inject = [
        '$element',
        'close',
        'bankResource',
        'registerPayResource',
        'SweetAlert',
        'localStorageService'
    ];

    angular
        .module('app')
        .controller('findPayModalController', findPayModalController);

})();