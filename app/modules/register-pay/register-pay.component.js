(function(){
    'use strict';

    angular
      .module('app')
      .component('registerPay', {
        templateUrl: 'app/modules/register-pay/register-pay.html',
        controller: 'registerPayController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();