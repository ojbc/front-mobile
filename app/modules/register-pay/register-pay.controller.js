(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Registro de pagos.                          |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function registerPayController(system, notification, restService, registerPayResource, SweetAlert, ModalService, $state, localStorageService, bankResource, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.idtransation = "";
        vm.loading = false;
        vm.form = "";
        vm.typeNotes = [
            { id: 1, nombre: 'Provincial' },
            { id: 2, nombre: 'Otros' }
        ];
        vm.types = [
            { id: 1, nombre: 'transferencia' },
            { id: 2, nombre: 'voucher' }
        ];
        vm.selectedInvoice = false;
        vm.campaign_select = true;
        vm.manager_select = true;
        vm.loading_gifts_manager = true;
        vm.invoice_select = true;
        if (typeof PermPermissionStore.getStore().registro !== 'undefined') {

        } else {
            $location.path('/home');
        }
        vm.userId = localStorageService.get('auth_userid');
        vm.companyId = localStorageService.get('auth_companyid');
        vm.d = new Date();

        vm.hour = vm.d.toLocaleTimeString();
        vm.dd = vm.d.getDate();
        vm.mm = vm.d.getMonth() + 1;
        vm.yyyy = vm.d.getFullYear();

        vm.form = {
            today: {
                opened: false,
            },
            transaction_date: {
                opened: true,
            },
        }
        vm.dateOption1 = {
            maxDate: new Date(),
            showWeeks: false
        }
        vm.today = {
                opened: false
            }
            /*------------------------------------end---------------------------------------*/
            /*----------------------------------Functions-----------------------------------*/
        vm.calendarOpen = function(test) {
            vm.today.opened = true;
        }

        vm.calendarOpen2 = function(calendar) {
            calendar.opened = false;
        }

        vm.returnValue = function() {
            vm.form.today = { opened3: false };
        }


        vm.defaultForm = function() {
            vm.form = {
                details: [],
                total_gifts: 0,
                rol: "analista",
                voucher: {
                    date: {
                        opened: false
                    }
                }
            };
            vm.gift = '';
            vm.gift_cantidad = '';
            vm.invoices = {};
        }
        vm.clearInputs = function(form) {
            vm.selectedInvoice = false;
            vm.campaign_select = true;
            vm.manager_select = true;
            vm.invoice_select = true;
            vm.payments = "";
            vm.form.transaction_date = "";
            vm.form = {
                    today: {
                        opened: false,
                    },
                    transaction_date: {
                        opened: true,
                    },
                },
                vm.dateOption1 = {
                    maxDate: new Date(),
                    showWeeks: false
                },
                vm.today = {
                    opened: false
                },
                vm.defaultForm();
            form.$setPristine();
        }
        vm.clearInputsget = function() {
            vm.selectedInvoice = false;
            vm.campaign_select = true;
            vm.manager_select = true;
            vm.invoice_select = true;
            vm.payments = "";
            vm.form = {
                    today: {
                        opened: false,
                    },
                    transaction_date: {
                        opened: true,
                    },
                },
                vm.dateOption1 = {
                    maxDate: new Date(),
                    showWeeks: false
                },
                vm.today = {
                    opened: false
                },
                vm.defaultForm();
            form.$setPristine();
        }
        vm.clearIn = function(form) {

            vm.payments = "";
            vm.form.referencia = "";
            vm.form.invoice = "";
            vm.form.bank = "";
            vm.form.date = "";
            vm.form.amount = "";

            vm.form.abono = 0;
            vm.form.cantvoucher = 0;
        }
        vm.clearRerence = function() {

            vm.form.referencia = "";
            vm.form.bank = "";
            vm.form.date = "";
            vm.form.amount = "";

        }
        vm.duplicateReferencePayment = function() {
            ModalService.showModal({
                templateUrl: "findPayModal",
                controller: "findPayModalController",
                controllerAs: "vm"
            }).then(function(modal) {
                // The modal object has the element built, if this is a bootstrap modal
                // you can call 'modal' to show it, if it's a custom modal just show or hide
                // it as you need to.
                modal.element.modal();
                modal.close.then(function(response) {
                    if (response) {
                        vm.form.bank = response.bank.name;
                        vm.form.date = response.date;
                        vm.form.amount = response.amount;

                    }
                });
            });
        }
        vm.duplicateDNIPayment = function() {
            ModalService.showModal({
                templateUrl: "DNIPayModal",
                controller: "DNIPayModalController",
                controllerAs: "vm",
            }).then(function(modal) {
                // The modal object has the element built, if this is a bootstrap modal
                // you can call 'modal' to show it, if it's a custom modal just show or hide
                // it as you need to.
                modal.element.modal();
                modal.close.then(function(response) {
                    if (response) {
                        vm.form.bank = response.bank.name;
                        vm.form.date = response.date;
                        vm.form.amount = response.amount;

                    }
                });
            });
        }

        vm.getchange = function() {

            vm.clearIn();

        }
        vm.selectInvoice = function(invoice) {

            vm.gift = '';
            vm.gift_cantidad = 0;
            vm.form.invoice = invoice;
            vm.getGiftsManager(vm.form.invoice.id, vm.form.manager.id);
            vm.getPayments(vm.form.invoice.id);
            vm.getRegisterpay(vm.form.invoice.id);
        }

        vm.getLimp = function() {
            vm.form.tipos = "";

        }

        vm.dateFormate = function(datetoday) {
                vm.subcharacter = datetoday;
                vm.number = vm.subcharacter.length;
                if (vm.number < 12) {
                    if ((vm.subcharacter[1] >= 0) && (vm.subcharacter[2] === '/') && (vm.subcharacter[5] != '/')) {
                        vm.subCadenaFirst = vm.subcharacter[0];
                        vm.subCadenaSecond = vm.subcharacter[1];
                        vm.subCadenatree = vm.subcharacter[2];
                        vm.subCadenaLast = vm.subcharacter.substring(11, 3)
                        vm.dateUpdate = vm.subCadenaFirst + vm.subCadenaSecond + vm.subCadenatree + '0' + vm.subCadenaLast;

                        return (vm.dateUpdate);

                    } else if ((vm.subcharacter[1] === '/') && (vm.subcharacter[3] >= 0)) {
                        vm.dateUpdate = '0' + vm.subcharacter;

                        return (vm.dateUpdate);

                    } else if ((vm.subcharacter[1] === '/') && (vm.subcharacter[3] === '/')) {
                        vm.subCadena = vm.subcharacter[0];
                        vm.subCadenaLast = vm.subcharacter.substring(10, 2)
                        vm.dateUpdate = '0' + vm.subCadena + '/0' + vm.subCadenaLast;

                        return (vm.dateUpdate);

                    } else {
                        vm.subcharacter;

                        return (vm.subcharacter);
                    }
                } else {
                    vm.caracter;

                    return (vm.caracter);
                }
            }
            /*----------------------------------------end--------------------------------------------*/
            /*--------------------------------------Function_GET-------------------------------------*/


        vm.getPayments = function(invoice) {
            vm.loading_manager = true;

            registerPayResource.payments.query({

                    invoice: invoice,
                    company_id: vm.companyId

                })
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.payments = response.data;
                        vm.loading_manager = false;
                        //console.log(response.data);
                    }
                });
        }



        vm.getRegisterList = function() {
            vm.loading_manager = true;
            registerPayResource.all.query()
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.register = response.data;
                        vm.loading_manager = false;
                        //console.log(response.data);
                    }
                });
        }

        vm.getRegisterpay = function(invoice) {
            vm.loading_manager = true;
            registerPayResource.invoice.query({
                    invoice: invoice,
                    company_id: vm.companyId

                })
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.payinvoice = response.data;
                        vm.loading_manager = false;
                        //console.log(response.data);
                        vm.form.abono = vm.payinvoice.monto;
                        vm.form.cantvoucher = vm.payinvoice.cantidad;

                    }
                });
        }





        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.save = function() {
            vm.loading = true;
            registerPayResource.save({
                    form_pay: '0',
                    bank: vm.form.bank,
                    reference: vm.form.referencia,
                    voucher_date: vm.form.date,
                    amount: vm.form.amount,
                    idtransation: vm.idtransation,
                    company_id: vm.companyId

                })
                .$promise.then(function(response) {
                vm.loading = false;
                    if (response.status === 200) {
                        localStorage.setItem('transactionid', response.data.id);
                        ModalService.showModal({
                            templateUrl: "finddescription",
                            controller: "finddescriptionController",
                            controllerAs: "vm",
                            inputs: {
                                transactionid: response.data.id
                            }
                        }).then(function(modal) {
                            modal.element.modal();


                            vm.clearInputsget();

                        });

                    }
                }, function(response) {
                vm.loading = false;
                    vm.motives = response.data.motives;
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives + '. Por favor intenta nuevamente!',
                        type: "error",
                    });

                });
        }

        vm.getReference = function(reference) {

            vm.loading_gifts = true;
            registerPayResource.referenceID({
                    reference: reference,
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {

                    if (response.status === 200) {
                        if (response.data.length === 1) {
                            vm.referencia = response.data[0];
                            vm.idtransation = vm.referencia.id;
                            vm.form.referencia = vm.referencia.reference;
                            vm.form.amount = vm.referencia.amount;
                            vm.form.date = vm.referencia.transaction_date;
                            vm.form.bank = vm.referencia.bank.bank_description;
                        } else {

                            ModalService.showModal({
                                templateUrl: "findPayModal",
                                controller: "findPayModalController",
                                controllerAs: "vm",
                            }).then(function(modal) {
                                modal.element.modal();
                                modal.close.then(function(response) {
                                    if (response) {

                                        vm.referencia = response.data[0];
                                        vm.idtransation = vm.referencia.id;
                                        vm.form.referencia = vm.referencia.reference;
                                        vm.form.amount = vm.referencia.amount;
                                        vm.form.date = vm.referencia.transaction_date;
                                        vm.form.bank = vm.referencia.bank.bank_description;
                                    }
                                });
                            });
                        }

                    }
                }, function(response) {

                    vm.motives = response.data.motives;
                    if (vm.motives === "Esta referencia esta duplicada debes ingresar los datos de manera manual") {
                        vm.duplicateReferencePayment();
                    }
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives + '. Por favor intenta nuevamente!',
                        type: "error",
                    });
                });
        }
        vm.searchCI = function(cdn) {

            vm.loading_gifts = true;
            registerPayResource.searchCDN({
                    cdn: cdn,
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {

                    if (response.status === 200) {
                        if (response.data.length === 1) {
                            vm.referencia = response.data[0];
                            vm.idtransation = vm.referencia.id;
                            vm.form.referencia = vm.referencia.reference;
                            vm.form.amount = vm.referencia.amount;
                            vm.form.date = vm.referencia.transaction_date;
                            vm.form.bank = vm.referencia.bank.bank_description;
                        }
                        if ((response.status === 200) && (response.data.length > 1)) {

                            ModalService.showModal({
                                templateUrl: "DNIPayModal",
                                controller: "DNIPayModalController",
                                controllerAs: "vm",
                                inputs: {
                                    ci: cdn
                                }
                            }).then(function(modal) {
                                modal.element.modal();
                                modal.close.then(function(response) {
                                    if (response) {

                                        vm.referencia = response.data[0];
                                        vm.idtransation = vm.referencia.id;
                                        vm.form.referencia = vm.referencia.reference;
                                        vm.form.amount = vm.referencia.amount;
                                        vm.form.date = vm.referencia.transaction_date;
                                        vm.form.bank = vm.referencia.bank.bank_description;
                                    }
                                });
                            });
                        }

                    }
                }, function(response) {
                    if (response.status === 400) {
                        SweetAlert.swal({
                            title: "Oops!",
                            text: "Los datos no coinciden.!",
                            type: "error",
                        });
                    } else {

                        vm.motives = response.data.motives;
                        SweetAlert.swal({
                            title: "Oops!",
                            text: response.data.motives + '. Por favor intenta nuevamente!',
                            type: "error",
                        });
                    }
                });
        }
        vm.searchVoucher = function() {
                vm.today = vm.form.transaction_date.toLocaleDateString();
                vm.dts = vm.dateFormate(vm.today);
            vm.loading = true;
                registerPayResource.searchVoucher({
                        form_pay: '0',
                        bank: vm.form.tipo.nombre,
                        reference: vm.form.referencias,
                        voucher_date: vm.dts,
                        amount: vm.form.amounts,
                        company_id: vm.companyId

                    })
                    .$promise.then(function(response) {
                    vm.loading = false;
                        if (response.status === 200) {
                            if (response.data.length === 1) {

                                vm.referencia = response.data[0];

                                vm.form.referencia = vm.referencia.reference;
                                vm.form.amount = vm.referencia.amount;
                                vm.form.date = vm.referencia.transaction_date;
                                vm.form.bank = vm.referencia.bank.bank_description;

                                registerPayResource.saveVoucher({
                                        form_pay: '0',
                                        bank: vm.form.bank,
                                        reference: vm.form.referencia,
                                        voucher_date: vm.form.date,
                                        amount: vm.form.amount,
                                        idtransation: vm.referencia.id,
                                        company_id: vm.companyId

                                    })
                                    .$promise.then(function(response) {

                                        if (response.status === 200) {

                                            if (response.status === 200) {
                                                localStorage.setItem('transactionid', response.data.id);
                                                ModalService.showModal({
                                                    templateUrl: "finddescription",
                                                    controller: "finddescriptionController",
                                                    controllerAs: "vm",
                                                    inputs: {
                                                        transactionid: response.data.id
                                                    }
                                                }).then(function(modal) {
                                                    modal.element.modal();


                                                    vm.clearInputsget();

                                                });

                                            }

                                        }
                                    }, function(response) {
                                        vm.motives = response.data.motives;
                                        SweetAlert.swal({
                                            title: "Oops!",
                                            text: response.data.motives + '. Por favor intenta nuevamente!',
                                            type: "error",
                                        });
                                        vm.clearInputsget();
                                    });
                            }

                        }
                    }, function(error) {
                    vm.loading = false;
                        vm.motives = error.data.motives;
                        SweetAlert.swal({
                            title: "Oops!",
                            text: error.data.motives + '. Por favor intenta nuevamente!',
                            type: "error",
                        });
                        vm.clearInputsget();
                    });
            }
            /*----------------------------------------end--------------------------------------------*/
            /*------------------------------------Function_Load--------------------------------------*/
        vm.defaultForm();
        vm.getRegisterList();
        /*----------------------------------------end--------------------------------------------*/
    }

    registerPayController.$inject = [
        'system',
        'notification',
        'restService',
        'registerPayResource',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        'bankResource',
        '$location',
        'PermPermissionStore'
    ];

    angular
        .module('app')
        .controller('registerPayController', registerPayController);


})();