(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo company.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function companyController(system, securityResource, SweetAlert, ModalService, $state, localStorageService, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // DEFAULTS
        vm.userId = localStorageService.get('auth_userid');
        vm.selected = true;
        vm.loading = false;
        if(typeof PermPermissionStore.getStore().superadmin  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/

        /*--------------------------------------Function_GET-------------------------------------*/
       
        vm.clearUser=function(){
            vm.form = {
             
            };
            
            vm.selected = true;
            vm.form={
                user:{
                    email:'',
                    profile:{
                        ci:'',
                        lastname: '',
                        name: ''
                    },

                },
                name:'',
                password: '',

            }
            
         }
         
         vm.activ =function(){
            vm.selected = true;
         }
         vm.getCompany = function() {
            vm.loading_company = true;

            securityResource.company.allUsers.query()
                .$promise.then(function(response) {
                   
                if(response.status === 200) {
                    vm.listCompany = response.data;
                    vm.loading_company = false;
                }
            });
        }
        vm.selectCompany = function(exc) {
            vm.selected = false;
            vm.form = angular.copy(exc);
           }
      
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/



        vm.save = function() {
            vm.loading = true;
            securityResource.company.save({
                     data:vm.form
                })
                .$promise.then(function(response) {
                vm.loading = false;
                if (response.status === 200) {
                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });
                    vm.getCompany();
                    vm.clearUser();
                   
                }else{
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives,
                        type: "error",
                    });
                }
            }, function(response) {
                vm.loading = false;
                if (response.status === 401) {
                  
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives,
                        type: "error",
                    });
                    vm.activ();
                   
                }
                
                system.managerErrors(response);
               
                vm.activ();
            });
        }
        vm.status = function(exc) {
            vm.loading_company = true;
            securityResource.company.status({
              company: exc.id,
              status: !exc.status
              }).$promise.then(function(response) {
                vm.loading_company = false;
                if (response.status === 200) {
                }
              }, function(response) {
                vm.loading_company = false;
                  SweetAlert.swal({
                    title: "Oops!",
                    text: 'Algo salio mal, intenta nuevamente!',
                    type: "error",
                  });
            });
        }
        

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
       
        vm.getCompany();
        /*----------------------------------------end--------------------------------------------*/
    }

    companyController.$inject = [
        'system',
        'securityResource',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        '$location', 
        'PermPermissionStore'

    ];

    angular
        .module('app')
        .controller('companyController', companyController);

})();