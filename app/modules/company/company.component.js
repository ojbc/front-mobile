(function(){
    'use strict';

    angular
        .module('app')
        .component('company', {
            templateUrl: 'app/modules/company/company.html',
            controller: 'companyController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();