(function(){
    'use strict';

    angular
      .module('app')
      .component('auth', {
        templateUrl: 'app/modules/auth/login.html',
        controller: 'authController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();