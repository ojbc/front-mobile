(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | logear del módulo Login el cual se encarga verificar si el                |
    | usuario existe para dar acceso al sistema.                                                          |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function authController( system, $auth, $window, $location, $rootScope, localStorageService, PermPermissionStore, PermRoleStore, SweetAlert, $log ) {
        /*-----------------------------------------Var------------------------------------------*/
        var vm = this;

        // DEFAULT
        
        /*-----------------------------------------end-------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        // funcion para logear usuarios
        vm.checkAccess = function () {
            // Ponemos a cargar el loader
            $rootScope.loader = true;

            // Le pasamos los parametros para loguear
            var params = {
                email: vm.form.user,
                password: vm.form.pass,
                remember: vm.form.remember
            };

            // Hacemos la autentificación
            $auth.login(params).then(function(response) {

                // ¿todo bien?
                if( response.status === 200 ){
                    var resp = response.data.data;

                    // Guardamos el token
                    $auth.setToken(resp.token);
                   
                    // Guardamos en el localStorage
                    localStorageService.set( 'auth_userid', resp.user_id );
                    localStorageService.set( 'auth_name', resp.name_id );
                    localStorageService.set( 'auth_role', resp.role );
                    localStorageService.set( 'auth_role_obj', resp.role_obj );
                    localStorageService.set( 'auth_permission', resp.permissions );
                    localStorageService.set( 'auth_companyid', resp.company_id );
                    
                    // Definimos los permisos
                    var permissions = [];

                    // Definimos el rol y le estipulamos los permisos
                    angular.forEach( resp.permissions, function (value, key) {
                        this.push( value.slug );
                    }, permissions);

                    PermRoleStore.defineRole( resp.role, permissions);

                    angular.forEach( resp.permissions, function (value, key) {
                        PermPermissionStore
                            .definePermission(value.slug, function () {
                                return true;
                            });
                    });

                    /*PermPermissionStore.defineManyPermissions(permissions, function (permissionName) {
                      return _.contains(permissions, permissionName);
                    });*/

                    // Mandamos al home
                    $location.path('/home');


                    // Refrescamos la pagina.
                   

                }else{
                    SweetAlert.swal({
                        title: "Oops!",
                        text:  response.data.motives,
                        type: "error",
                    });
                };

            }).catch(function(response) {
                // ocultamos el loader
                $rootScope.loader = false;
                SweetAlert.swal({
                    title: "Oops!",
                    text: response.data.motives,
                    type: "error",
                });
                  // Mostramos los errores
                  //system.errors.resources( response );
                system.notify.error( response.data.error_message );
            });
        }
        /*----------------------------------------end--------------------------------------------*/
    }

    authController.$inject = [
      'system',
      '$auth',
      '$window',
      '$location',
      '$rootScope',
      'localStorageService',
      'PermPermissionStore',
      'PermRoleStore',
      'SweetAlert',
      '$log'
    ];

    angular
      .module('app')
      .controller('authController', authController);

    })();