(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo profile.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function profileController(system, securityResource, SweetAlert, ModalService, $state, localStorageService, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // DEFAULTS
        vm.userId = localStorageService.get('auth_userid');
        vm.loading = false;
        if((typeof PermPermissionStore.getStore().superadmin  !== 'undefined')|| (typeof PermPermissionStore.getStore().profile  !== 'undefined')){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/

        /*--------------------------------------Function_GET-------------------------------------*/
        vm.getProfiles = function () {
            vm.loading = true;
            securityResource.users.profiles.query()
                .$promise.then(function(response) {
                vm.loading = false;
                if(response.status === 200) {
                    vm.form = response.data[0];
                }
            });
        }

        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/



        vm.save = function() {
            vm.loading = true;
            securityResource.users.saveUpdate(vm.form)
                .$promise.then(function(response) {
                vm.loading = false;
                if (response.status === 200) {

                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });
                    vm.getProfiles();

                }else{
                    system.managerErrors(response);
                }
            }, function(response) {
                vm.loading = false;
                system.managerErrors(response);
            });
        }

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getProfiles();
        /*----------------------------------------end--------------------------------------------*/
    }

    profileController.$inject = [
        'system',
        'securityResource',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        '$location', 
        'PermPermissionStore'

    ];

    angular
        .module('app')
        .controller('profileController', profileController)
        .directive('numbersDay', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    var validateNumber = function (inputValue) {
                        var maxLength = 2;
                        if (attrs.max) {
                            maxLength = attrs.max;
                        }
                        if (inputValue === undefined) {
                            return '';
                        }
                        var transformedInput = inputValue.replace(/[^1-31]/g, '');
                        if (transformedInput !== inputValue) {
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        if (transformedInput.length > maxLength) {
                            transformedInput = transformedInput.substring(0, maxLength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        var isNotEmpty = (transformedInput.length === 0) ? true : false;
                        ctrl.$setValidity('notEmpty', isNotEmpty);
                        return transformedInput;
                    };

                    ctrl.$parsers.unshift(validateNumber);
                    ctrl.$parsers.push(validateNumber);
                    attrs.$observe('notEmpty', function () {
                        validateNumber(ctrl.$viewValue);
                    });
                }
            };
        })
         .directive('numbersYear', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    var validateNumber = function (inputValue) {
                        var maxLength = 4;
                        if (attrs.max) {
                            maxLength = attrs.max;
                        }
                        if (inputValue === undefined) {
                            return '';
                        }
                        var transformedInput = inputValue.replace(/[^1900-2010]/g, '');
                        if (transformedInput !== inputValue) {
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        if (transformedInput.length > maxLength) {
                            transformedInput = transformedInput.substring(0, maxLength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        var isNotEmpty = (transformedInput.length === 0) ? true : false;
                        ctrl.$setValidity('notEmpty', isNotEmpty);
                        return transformedInput;
                    };

                    ctrl.$parsers.unshift(validateNumber);
                    ctrl.$parsers.push(validateNumber);
                    attrs.$observe('notEmpty', function () {
                        validateNumber(ctrl.$viewValue);
                    });
                }
            };
        })
        .directive('numbersMonth', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    var validateNumber = function (inputValue) {
                        
                        var maxLength = 2;
                        if (attrs.max) {
                            maxLength = attrs.max;
                        }
                        if (inputValue === undefined) {
                            return '';
                        }
                        var transformedInput = inputValue.replace(/[^1-12]/, '');
                        if (transformedInput !== inputValue) {
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        if (transformedInput.length > maxLength) {
                            transformedInput = transformedInput.substring(0, maxLength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        var isNotEmpty = (transformedInput.length === 0) ? true : false;
                        ctrl.$setValidity('notEmpty', isNotEmpty);
                        return transformedInput;
                    };

                    ctrl.$parsers.unshift(validateNumber);
                    ctrl.$parsers.push(validateNumber);
                    attrs.$observe('notEmpty', function () {
                        validateNumber(ctrl.$viewValue);
                    });
                }
            };
        });

})();
