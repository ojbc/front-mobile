(function(){
    'use strict';

    angular
        .module('app')
        .component('profile', {
            templateUrl: 'app/modules/profile/profile.html',
            controller: 'profileController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();