(function(){
    'use strict';

    angular
      .module('app')
      .component('bank', {
        templateUrl: 'app/modules/bank/bank.html',
        controller: 'bankController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();