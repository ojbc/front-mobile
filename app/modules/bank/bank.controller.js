(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Bancos.                                     |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function bankController( system, bankResource, SweetAlert ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.selectedBank = false;
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
        vm.clearInputs = function(form) {
          vm.form = {};
          form.$setPristine();
          vm.selectedBank = false;
        }

        vm.selectBank = function(bank) {
          vm.selectedBank = true;
          vm.form = angular.copy(bank);
        }
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/
        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.all.query()
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listBanks = response.data;
                    vm.loading_banks = false;
                }
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.save = function() {
        //console.log(vm.form);
            vm.loading = true;
        bankResource.save(vm.form)
          .$promise.then(function(response) {
            vm.loading = false;
            if (response.status === 200) {
                //console.log(response.data);
                SweetAlert.swal({
                  title: "Excelente!",
                  text: 'Operación realizada correctamente!',
                  type: "success",
                });

                vm.getBanks();
                vm.clearInputs();

            }
          }, function(response) {
            vm.loading = false;
              //console.log(response.data);
              SweetAlert.swal({
                title: "Oops!",
                text: 'Algo salio mal, intenta nuevamente!',
                type: "error",
              });
          });
        }

        vm.delete = function() {
          system.confirm(
            'Estas a punto de borrar un banco',
            function(is_confirm) {
              if( is_confirm === true ) {
                bankResource.delete({
                    id: vm.form.id
                  }).$promise.then(function(response) {
                    if (response.status == 200) {
                      SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada exitosamente!',
                        type: "success",
                      });

                      vm.getBanks();
                      vm.clearInputs();
                    }

                    SweetAlert.swal({
                      title: "Oops!",
                      text: "Algo ha salido mal, intente nuevamente",
                      type: "error",
                    });
                  }, function(response){
                    if( response.status == 401 ) {
                      SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives[0],
                        type: "error",
                      });
                    }
                });
              }
            }
          );
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/

        vm.getBanks();
        /*----------------------------------------end--------------------------------------------*/
    }

    bankController.$inject = [
      'system',
      'bankResource',
      'SweetAlert'
    ];

    angular
      .module('app')
      .controller('bankController', bankController);

})();