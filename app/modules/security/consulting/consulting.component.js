(function(){
    'use strict';

    angular
      .module('app')
      .component('consulting', {
        templateUrl: 'app/modules/security/consulting/consulting.html',
        controller: 'consultingController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();