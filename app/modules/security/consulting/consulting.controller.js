(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta del módulo Historico.                                            |
    ------------------------------------end-------------------------------------*/

    /** @ngInject */
    function consultingController( system, localStorageService, $location, PermPermissionStore, ApiUrl, services, $auth, $scope) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.companyId = localStorageService.get('auth_companyid');
        if(typeof PermPermissionStore.getStore().seguridad  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
       
        $scope.tempHtml = '';

       
        vm.dtOptions = {

            scrollX: true,
            destroy: true,
            cache: false,
            processing: true,
            serverSide: true,
            sorting: [[0, 'desc']],
            ajax: {
                url: ApiUrl + services.security.consulting.paginate,
                type: "GET",
                data: function(d) {
                    d.company_id= vm.companyId;
                  
                },
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                }
                
            },
            columns: [
                {data: "full_name"},
                { data: "module" },
                { data: "type_operation" },
                { data: "details" },
                { data: "created_at" }
            
        ],
            language: {
                sLengthMenu:     "Mostrar  _MENU_",
                sZeroRecords:    "No se encontraron resultados",
                sEmptyTable:     "Ningún dato disponible en esta tabla",
                sInfo:           "Del _START_ al _END_",
                sInfoEmpty:      "0 Registros",
                sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                sInfoPostFix:    "",
                sSearch:         "Buscar: ",
                sUrl:            "",
                sInfoThousands:  ",",
                sLoadingRecords: "Cargando...",
                sProcessing: "Cargando",
                oPaginate: {
                    sFirst:    "Primero",
                    sLast:     "Último",
                    sNext:     "Siguiente",
                    sPrevious: "Anterior"
                },
                oAria: {
                    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
        };

        vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
        /*----------------------------------------end--------------------------------------------*/
    }

    consultingController.$inject = [
        'system',
        'localStorageService',
        '$location', 
        'PermPermissionStore', 
        'ApiUrl', 
        'services', 
        '$auth', 
        '$scope'
    ];

    angular
        .module('app')
        .controller('consultingController', consultingController);

})();