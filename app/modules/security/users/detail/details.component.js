(function(){
    'use strict';

    angular
      .module('app')
      .component('usersDetails', {
        templateUrl: 'app/modules/security/users/detail/details.html',
        controller: 'usersDetailsController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();