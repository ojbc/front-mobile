(function(){
    'use strict';

    /** @ngInject */
    function usersDetailsController( system, securityResource, ModalService, $stateParams, $location, SweetAlert, PermPermissionStore) {
      var vm = this;
      var user_id;
      if(typeof PermPermissionStore.getStore().seguridad  !== 'undefined'){

      }else{
          $location.path('/home');
      }
      vm.getUserInfo = function( user_id ) {
          vm.loading_details = true;

          securityResource.users.getFullDetails.query({
            user_id: user_id
          }).$promise.then(function(response) {
              if (response.status === 200) {
                vm.user = response.data;
                vm.loading_details = false;
                  vm.form = response.data;
              }
            },function(response) {
              if (response.status === 401) {

                vm.loading_details = false;

              }

            });

      }

      vm.getListRoles = function () {
        vm.loading_roles = true;
        securityResource.roles.all.query()
        .$promise.then(function(response) {
          if (response.status === 200) {
            vm.listRoles = response.data;
            vm.loading_roles = false;

          }
        });
      }
        vm.save = function() {

            securityResource.users.saveUpdate(vm.form)
                .$promise.then(function(response) {
                if (response.status === 200) {
                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });
                    vm.getProfiles();

                }else{
                    system.managerErrors(response);
                }
            }, function(response) {
                system.managerErrors(response);
            });
        }
      vm.changeUserRole = function (role) {

        securityResource.users.changeRole({
          role: role.slug,
          user: $stateParams.id
        }).$promise.then(function(response) {
          if (response.status === 200) {
            vm.getUserInfo($stateParams.id);
          }
        });
      }

      if( $stateParams.id ) {
        vm.getUserInfo($stateParams.id);
        vm.getListRoles();
      } else {
        $location.path('/home');
      }
    }

    usersDetailsController.$inject = [
      'system',
      'securityResource',
      'ModalService',
      '$stateParams',
      '$location',
      'SweetAlert',
      'PermPermissionStore'
    ];

    angular
      .module('app')
      .controller('usersDetailsController', usersDetailsController)
      .directive('numbersDay', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                var validateNumber = function (inputValue) {
                    var maxLength = 2;
                    if (attrs.max) {
                        maxLength = attrs.max;
                    }
                    if (inputValue === undefined) {
                        return '';
                    }
                    var transformedInput = inputValue.replace(/[^1-31]/g, '');
                    if (transformedInput !== inputValue) {
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    if (transformedInput.length > maxLength) {
                        transformedInput = transformedInput.substring(0, maxLength);
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    var isNotEmpty = (transformedInput.length === 0) ? true : false;
                    ctrl.$setValidity('notEmpty', isNotEmpty);
                    return transformedInput;
                };

                ctrl.$parsers.unshift(validateNumber);
                ctrl.$parsers.push(validateNumber);
                attrs.$observe('notEmpty', function () {
                    validateNumber(ctrl.$viewValue);
                });
            }
        };
    })
     .directive('numbersYear', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                var validateNumber = function (inputValue) {
                    var maxLength = 4;
                    if (attrs.max) {
                        maxLength = attrs.max;
                    }
                    if (inputValue === undefined) {
                        return '';
                    }
                    var transformedInput = inputValue.replace(/[^1900-2010]/g, '');
                    if (transformedInput !== inputValue) {
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    if (transformedInput.length > maxLength) {
                        transformedInput = transformedInput.substring(0, maxLength);
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    var isNotEmpty = (transformedInput.length === 0) ? true : false;
                    ctrl.$setValidity('notEmpty', isNotEmpty);
                    return transformedInput;
                };

                ctrl.$parsers.unshift(validateNumber);
                ctrl.$parsers.push(validateNumber);
                attrs.$observe('notEmpty', function () {
                    validateNumber(ctrl.$viewValue);
                });
            }
        };
    })
    .directive('numbersMonth', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                var validateNumber = function (inputValue) {
                    var maxLength = 2;
                    if (attrs.max) {
                        maxLength = attrs.max;
                    }
                    if (inputValue === undefined) {
                        return '';
                    }
                    var transformedInput = inputValue.replace(/[^1-12]/g, '');
                    if (transformedInput !== inputValue) {
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    if (transformedInput.length > maxLength) {
                        transformedInput = transformedInput.substring(0, maxLength);
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    var isNotEmpty = (transformedInput.length === 0) ? true : false;
                    ctrl.$setValidity('notEmpty', isNotEmpty);
                    return transformedInput;
                };

                ctrl.$parsers.unshift(validateNumber);
                ctrl.$parsers.push(validateNumber);
                attrs.$observe('notEmpty', function () {
                    validateNumber(ctrl.$viewValue);
                });
            }
        };
    });

})();