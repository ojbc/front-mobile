(function(){
    'use strict';
    
    angular
      .module('app')
      .component('users', {
        templateUrl: 'app/modules/security/users/users.html',
        controller: 'usersController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();