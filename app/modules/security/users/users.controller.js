(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Usuarios.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function usersController( system, securityResource, ModalService, SweetAlert, localStorageService, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // DEFAULTS
        vm.loading_roles = true;
        vm.companyId = localStorageService.get('auth_companyid');
        if(typeof PermPermissionStore.getStore().seguridad  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
        securityResource.roles.all.query()
            .$promise.then(function(response) {
            if (response.status === 200) {
                vm.listRoles = response.data;
                vm.loading_roles = false;
                //console.log(response.data);
            }
        });
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/

        vm.getListUsers = function () {
            vm.loading_users = true;
            securityResource.users.all.query({ 
                company_id: vm.companyId
            })
                .$promise.then(function(response) {
                if (response.status === 200) {
                    vm.listUsers = response.data;
                    vm.loading_users = false;
                    //console.log(response.data);
                }
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.agregateUser = function () {
            ModalService.showModal({
                templateUrl: "addingUserModal",
                controller: "addingUserModalController",
                controllerAs: "vm"
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(response) {
                    if (response.status === 200) {
                        vm.getListUsers();
                    }
                });
            });
        }

        vm.changeUserRole = function (role, user) {
           
            securityResource.users.changeRole({
                role: role.slug,
                user: user.id
            })
                .$promise.then(function(response) {
                if (response.status === 200) {
                   
                    vm.getListUsers();
                }
            })
        }

        vm.deleteUser = function( user ) {
            system.confirm(
                'Estas a punto de borrar un usuario',
                function(is_confirm) {
                    if( is_confirm === true ) {
                        securityResource
                            .users.delete({
                            user: user.id
                        }).$promise.then(function(response) {
                            if (response.status == 200) {
                                SweetAlert.swal({
                                    title: "Excelente!",
                                    text: 'Usuario borrado exitosamente!',
                                    type: "success",
                                });

                                vm.getListUsers();
                            }
                        }, function(response){
                            if( response.status == 401 ) {
                                SweetAlert.swal({
                                    title: "Oops!",
                                    text: response.data.motives[0],
                                    type: "error",
                                });
                            }
                        });
                    }
                }
            );
        }
        vm.status = function(user) {
            securityResource.users.status({
              user: user.id,
              status: !user.status
              }).$promise.then(function(response) {
                if (response.status === 200) {
                }
              }, function(response) {
                  SweetAlert.swal({
                    title: "Oops!",
                    text: 'Algo salio mal, intenta nuevamente!',
                    type: "error",
                  });
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getListUsers();
        /*----------------------------------------end--------------------------------------------*/
    }

    usersController.$inject = [
        'system',
        'securityResource',
        'ModalService',
        'SweetAlert',
        'localStorageService',
        '$location', 
        'PermPermissionStore'
    ];

    angular
        .module('app')
        .controller('usersController', usersController);

})();
