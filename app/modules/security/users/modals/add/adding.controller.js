(function(){
    'use strict';

    /** @ngInject */
    function addingUserModalController( system, securityResource, $element, close ) {
        var vm = this;

        vm.form = {
            sexo: 'female'
        };

        vm.loading_roles = true;

        securityResource.roles.all.query()
            .$promise.then(function(response) {
            if (response.status === 200) {
                vm.listRoles = response.data;
                vm.loading_roles = false;
            }
        });

        vm.createUser = function () {
            securityResource.users.save(vm.form)
                .$promise.then(function(response) {
                if (response.status === 200) {

                    //  Manually hide the modal using bootstrap.
                    $element.modal('hide');
                    close( response, 200 );
                }
            },function(response){
                if( response.status === 400 ) {

                }
                if( response.status === 401 ) {

                }
            });
        }
    }

    addingUserModalController.$inject = [
        'system',
        'securityResource',
        '$element',
        'close'
    ];

    angular
        .module('app')
        .controller('addingUserModalController', addingUserModalController)
        .directive('numbersDay', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    var validateNumber = function (inputValue) {
                        var maxLength = 2;
                        if (attrs.max) {
                            maxLength = attrs.max;
                        }
                        if (inputValue === undefined) {
                            return '';
                        }
                        var transformedInput = inputValue.replace(/[^1-31]/g, '');
                        if (transformedInput !== inputValue) {
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        if (transformedInput.length > maxLength) {
                            transformedInput = transformedInput.substring(0, maxLength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        var isNotEmpty = (transformedInput.length === 0) ? true : false;
                        ctrl.$setValidity('notEmpty', isNotEmpty);
                        return transformedInput;
                    };
    
                    ctrl.$parsers.unshift(validateNumber);
                    ctrl.$parsers.push(validateNumber);
                    attrs.$observe('notEmpty', function () {
                        validateNumber(ctrl.$viewValue);
                    });
                }
            };
        })
         .directive('numbersYear', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    var validateNumber = function (inputValue) {
                        var maxLength = 4;
                        if (attrs.max) {
                            maxLength = attrs.max;
                        }
                        if (inputValue === undefined) {
                            return '';
                        }
                        var transformedInput = inputValue.replace(/[^1900-2010]/g, '');
                        if (transformedInput !== inputValue) {
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        if (transformedInput.length > maxLength) {
                            transformedInput = transformedInput.substring(0, maxLength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        var isNotEmpty = (transformedInput.length === 0) ? true : false;
                        ctrl.$setValidity('notEmpty', isNotEmpty);
                        return transformedInput;
                    };
    
                    ctrl.$parsers.unshift(validateNumber);
                    ctrl.$parsers.push(validateNumber);
                    attrs.$observe('notEmpty', function () {
                        validateNumber(ctrl.$viewValue);
                    });
                }
            };
        })
        .directive('numbersMonth', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    var validateNumber = function (inputValue) {
                        var maxLength = 2;
                        if (attrs.max) {
                            maxLength = attrs.max;
                        }
                        if (inputValue === undefined) {
                            return '';
                        }
                        var transformedInput = inputValue.replace(/^[1-12]/g, '');
                        if (transformedInput !== inputValue) {
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        if (transformedInput.length > maxLength) {
                            transformedInput = transformedInput.substring(0, maxLength);
                            ctrl.$setViewValue(transformedInput);
                            ctrl.$render();
                        }
                        var isNotEmpty = (transformedInput.length === 0) ? true : false;
                        ctrl.$setValidity('notEmpty', isNotEmpty);
                        return transformedInput;
                    };
    
                    ctrl.$parsers.unshift(validateNumber);
                    ctrl.$parsers.push(validateNumber);
                    attrs.$observe('notEmpty', function () {
                        validateNumber(ctrl.$viewValue);
                    });
                }
            };
        });

})();
