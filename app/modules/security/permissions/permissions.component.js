(function(){
    'use strict';

    angular
      .module('app')
      .component('permissions', {
        templateUrl: 'app/modules/security/permissions/permissions.html',
        controller: 'permissionsController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();