(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | registro permiso.                                                         |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function addingPermissionModalController( $element, system, securityResource, Slug, form, close ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // Default
        vm.form = form;
        vm.updating = false;
        // Si el formulario trae informacion
        // significa que estamos modificando un registro
        if( vm.form.name.length > 0 ) {
            vm.updating = true;
        }
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
        vm.save = function () {
            securityResource.permission.save(vm.form)
                .$promise.then(function(response) {
                if (response.status === 200) {
                    //console.log(response.data);

                    //  Manually hide the modal using bootstrap.
                    $element.modal('hide');
                    close( response, 200 );
                }
            });
        }

        vm.getSlug = function() {
            return vm.form.slug = Slug.slugify(vm.form.name);
        }
        /*----------------------------------------end--------------------------------------------*/
    }

    addingPermissionModalController.$inject = [
        '$element',
        'system',
        'securityResource',
        'Slug',
        'form',
        'close'
    ];

    angular
        .module('app')
        .controller('addingPermissionModalController', addingPermissionModalController);

})();