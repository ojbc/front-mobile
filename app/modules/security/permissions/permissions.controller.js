(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Permisos de usuarios.                       |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function permissionsController( system, securityResource, ModalService, $location ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/
        vm.openModal = function (form) {
            ModalService.showModal({
                templateUrl: "addingPermissionModal",
                controller: "addingPermissionModalController",
                controllerAs: "vm",
                inputs: {
                    form: form
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(response) {
                    //console.log( response );
                    if (response.status === 200) {
                        //
                        vm.allPermission();
                    }
                });
            });
        }

        vm.agregatePermission = function () {
            var form = {
                name: '',
                description: '',
                slug: ''
            };

            vm.openModal(form);
        }

        vm.updatePermission = function (_form) {
            var data = {
                name: _form.name,
                description: _form.description,
                slug: _form.slug,
                old: _form
            };

            vm.openModal(data);
        }
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/
        vm.allPermission = function () {
            vm.loading_permission = true;
            securityResource.permission.all.query()
                .$promise.then(function(response) {
                if (response.status === 200) {
                    vm.listPermission = response.data;
                    vm.loading_permission = false;
                    //console.log(response.data);
                }
            })
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.allPermission();
        /*----------------------------------------end--------------------------------------------*/
    }

    permissionsController.$inject = [
        'system',
        'securityResource',
        'ModalService',
        '$location'
    ];

    angular
        .module('app')
        .controller('permissionsController', permissionsController);

})();