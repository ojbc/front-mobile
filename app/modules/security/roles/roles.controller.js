(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Rol de usuarios.                            |
    ------------------------------------end-------------------------------------*/

    /** @ngInject */
    function rolesController( system, securityResource, ModalService, $location ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/

        vm.agregateRole = function () {
            var form = {
                name: '',
                description: ''
            };

            vm.openModal(form);
        }

        vm.updateRole = function (_form) {
            vm.openModal(_form);
        }
        /*----------------------------------------end--------------------------------------------*/
        /*--------------------------------------Function_GET-------------------------------------*/

        vm.getRolePermission = function( role_slug ) {
            vm.loading_permission = true;
            securityResource.roles.getPermission.query({
                slug: role_slug
            })
                .$promise.then(function(response) {
                if (response.status === 200) {
                    vm.listPermission = response.data;
                    vm.loading_permission = false;
                    vm.selectedRole = role_slug;
                    //console.log(response.data);
                }
            })
        }

        vm.getRolesList = function () {
            vm.loading_roles = true;
            securityResource.roles.all.query()
                .$promise.then(function(response) {
                if (response.status === 200) {
                    vm.listRoles = response.data;
                    vm.loading_roles = false;
                    //console.log(response.data);
                }
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.openModal = function (form) {
            ModalService.showModal({
                templateUrl: "addingRoleModal",
                controller: "addingRoleModalController",
                controllerAs: "vm",
                inputs: {
                    form: form
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(response) {
                    if (response.status === 200) {
                        //
                        vm.getRolesList();
                        vm.getRolePermission(response.data.slug);
                    }
                });
            });
        }

        vm.updatePermissionRole = function (perm) {
            securityResource.roles.updatePermission({
                perm: perm,
                slug: vm.selectedRole
            }).$promise.then(function(response) {
                if (response.status === 200) {
                  
                }
            });
        }
        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getRolesList();
        vm.getRolePermission('administrador');
        /*----------------------------------------end--------------------------------------------*/
    }

    rolesController.$inject = [
        'system',
        'securityResource',
        'ModalService',
        '$location'
    ];

    angular
        .module('app')
        .controller('rolesController', rolesController);

})();