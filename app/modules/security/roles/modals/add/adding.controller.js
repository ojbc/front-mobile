(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro rol de usuario rol de usuario.                                                    |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function addingRoleModalController( $element, system, securityResource, close, form ) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;

        // Default
        vm.form = form;
        vm.updating = false;
        // Si el formulario trae informacion
        // significa que estamos modificando un registro
        if( vm.form.name.length > 0 ) {
            vm.updating = true;
        }
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        //funcion para guardar el registro
        vm.save = function () {
            //console.log(vm.form)
            securityResource.roles.save(vm.form)
                .$promise.then(function(response) {
                if (response.status === 200) {
                    //console.log(response.data);

                    //  Manually hide the modal using bootstrap.
                    $element.modal('hide');
                    close( response, 200 );
                }
            },function(response){
                if( response.status === 400 ) {
                   
                }
                if( response.status === 401 ) {
                   
                }
            });
        }
    }

    addingRoleModalController.$inject = [
        '$element',
        'system',
        'securityResource',
        'close',
        'form'
    ];

    angular
        .module('app')
        .controller('addingRoleModalController', addingRoleModalController);

})();