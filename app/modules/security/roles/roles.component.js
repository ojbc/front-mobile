(function(){
    'use strict';

    angular
      .module('app')
      .component('roles', {
        templateUrl: 'app/modules/security/roles/roles.html',
        controller: 'rolesController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();