(function(){
    'use strict';
    
    angular
      .module('app')
      .component('search', {
        templateUrl: '/app/modules/search/search.html',
        controller: 'searchController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();