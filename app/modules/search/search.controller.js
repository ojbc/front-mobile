(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Importar Data Banco.                        |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function searchController(system, bankResource, ApiUrl, $auth, localStorageService, $location, PermPermissionStore, services, $scope, $timeout) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.d = new Date();
        vm.loading_register = false;


        vm.loading = false;
        vm.companyId = localStorageService.get('auth_companyid');
        if((typeof PermPermissionStore.getStore().carga !== 'undefined') || (typeof PermPermissionStore.getStore().cargas  !== 'undefined')){

        }else{
            $location.path('/home');
        }
        vm.form = {
            elements:"",
            bankselect:"",
             
         }


        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/


        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.all.query({
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {

                    if (response.status === 200) {
                        vm.listBanks = response.data;
                        vm.loading_banks = false;
                    }
                });
        }

        vm.getBanks();
        
        vm.clearInputs = function(form) {
            vm.form = {};
            form.$setPristine();
            vm.form = {
               elements:"",
               bankselect:"",
                
            }
            
            vm.getBanks();
           vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
           
        }
  
        


        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
   
        $scope.tempHtml = '';

       
        vm.dtOptions = {

            scrollX: true,
            destroy: true,
            cache: false,
            processing: true,
            serverSide: true,
            sorting: [[0, 'desc']],
            ajax: {
                url: ApiUrl + services.advancedsearch.search,
                type: "GET",
                data: function(d) {
                    d.element=vm.form.element;
                    d.company_id=vm.companyId;
                    if(vm.form.bankselect){
                        d.name_bank=vm.form.bankselect.banks.name;
                    }
                    
                  
                },
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                }
                
            },
            columns: [
            { data: "id"},
            { data: "transaction_date"},
            { data: "reference"},
            { data: "amount"},
            { data: "description"},
            
        ],
            language: {
                sLengthMenu:     "Mostrar  _MENU_",
                sZeroRecords:    "No se encontraron resultados",
                sEmptyTable:     "Ningún dato disponible en esta tabla",
                sInfo:           "Del _START_ al _END_",
                sInfoEmpty:      "0 Registros",
                sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                sInfoPostFix:    "",
                sSearch:         "Buscar: ",
                sUrl:            "",
                sInfoThousands:  ",",
                sLoadingRecords: "Cargando...",
                sProcessing: "Cargando",
                oPaginate: {
                    sFirst:    "Primero",
                    sLast:     "Último",
                    sNext:     "Siguiente",
                    sPrevious: "Anterior"
                },
                oAria: {
                    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
        };

        vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
       
        vm.search = function() {
           
            vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
           
        }


        //  vm.getTransaction();
        /*----------------------------------------end--------------------------------------------*/


    }

    searchController.$inject = [
        'system',
        'bankResource',
        'ApiUrl',
        '$auth',
        'localStorageService',
        '$location',
        'PermPermissionStore',
         'services', 
         '$scope',
         '$timeout'
    ];

    angular
        .module('app')
        .controller('searchController', searchController);

})();