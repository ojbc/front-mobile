(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Importar Data Banco.                        |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function IDBController(system, bankResource, ApiUrl, $auth, $state, notification, restService, ImportingDataResource, SweetAlert, securityResource, localStorageService, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.d = new Date();
        vm.loading_register = false;
        vm.hour = vm.d.toLocaleTimeString();
        vm.loadings = false;
        vm.dd = vm.d.getDate();
        vm.mm = vm.d.getMonth() + 1;
        vm.yyyy = vm.d.getFullYear();
        vm.$onInit = activate;

        vm.companyId = localStorageService.get('auth_companyid');
        if (typeof PermPermissionStore.getStore().importar !== 'undefined') {

        } else {
            $location.path('/home');
        }
        vm.form = {
            today: {
                opened: false,
            },
            transaction_date: {
                opened: true,
            },
        }
        vm.dateOption1 = {
            maxDate: new Date(),
            showWeeks: false
        }
        vm.today = {
            opened: false
        }

        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/

        vm.loading = function() {

        }
        vm.calendarOpen = function(test) {
            vm.today.opened = true;
        }

        vm.calendarOpen2 = function(calendar) {
            calendar.opened = false;
        }

        vm.returnValue = function() {
            vm.form.today = { opened3: false };
        }
        vm.clearTable = function() {
            vm.resultransactions = [];
        }
        vm.formateDate = function(today) {
            vm.d = today;
            vm.dd = vm.d.getDate();
            vm.mm = vm.d.getMonth() + 1;
            vm.yyyy = vm.d.getFullYear();
            if (vm.dd < 10) {
                vm.dd = '0' + vm.dd;
            }
            if (vm.mm < 10) {
                vm.mm = '0' + vm.mm;
            }
            vm.time = vm.dd + '/' + vm.mm + '/' + vm.yyyy;
        }

        vm.dateFormate = function(datetoday) {
            vm.subcharacter = datetoday;
            vm.number = vm.subcharacter.length;
            if (vm.number < 12) {
                if ((vm.subcharacter[1] >= 0) && (vm.subcharacter[2] === '/') && (vm.subcharacter[5] != '/')) {
                    vm.subCadenaFirst = vm.subcharacter[0];
                    vm.subCadenaSecond = vm.subcharacter[1];
                    vm.subCadenatree = vm.subcharacter[2];
                    vm.subCadenaLast = vm.subcharacter.substring(11, 3)
                    vm.dateUpdate = vm.subCadenaFirst + vm.subCadenaSecond + vm.subCadenatree + '0' + vm.subCadenaLast;

                    return (vm.dateUpdate);

                } else if ((vm.subcharacter[1] === '/') && (vm.subcharacter[3] >= 0)) {
                    vm.dateUpdate = '0' + vm.subcharacter;

                    return (vm.dateUpdate);

                } else if ((vm.subcharacter[1] === '/') && (vm.subcharacter[3] === '/')) {
                    vm.subCadena = vm.subcharacter[0];
                    vm.subCadenaLast = vm.subcharacter.substring(10, 2)
                    vm.dateUpdate = '0' + vm.subCadena + '/0' + vm.subCadenaLast;

                    return (vm.dateUpdate);

                } else {
                    vm.subcharacter;

                    return (vm.subcharacter);
                }
            } else {
                vm.caracter;

                return (vm.caracter);
            }
        }
        vm.getClear = function() {
            vm.form.reference = "";
            vm.form.bankselect = "";
            vm.form.transaction_date = "";
            vm.form.today = {
                opened2: false,
            };
            vm.form.amount = "";
        }
        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.all.query({
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {

                    if (response.status === 200) {
                        vm.listBanks = response.data;
                        vm.loading_banks = false;
                    }
                });
        }

        vm.getBanks();

        function activate() {
            vm.form = {};


            vm.resultransactions = [];

            vm.dzOptions = {
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                },
                method: 'post',
                url: ApiUrl + '/importing-data-bank/processing-data-bank',
                paramName: 'archive',
                maxFiles: '1',
                acceptedFiles: '.txt, .xls, .xlsx',
                addRemoveLinks: true,
                autoProcessQueue: false,
                dictDefaultMessage: '<div class="col-md-auto col-xs-auto" class="icon"><span class="mdi mdi-cloud-upload"></span></div>' +
                    '<h6>Arrastra hasta aquí o Haz click para subir un archivo.</h6>',
                dictRemoveFile: 'Eliminar archivo',
                dictResponseError: 'No se pudo subir el archivo.',
                dictInvalidFileType: 'No puedes subir archivos de esté tipo.',
                dictMaxFilesExceeded: 'Solo puedes subir un solo archivo a la vez.'
            };

            vm.dzMethods = {};

            vm.paneldata = {
                typetransactions: [{
                        name: 'Importar Data',
                        code: 'masiveupload',
                        id: 1
                    },
                    {
                        name: 'Bloq / Desbloq. Deposito',
                        code: 'lockunlock',
                        id: 2
                    }
                ]
            };

            vm.form = {
                operationselect: vm.paneldata.typetransactions[0]
            }
        }
        //Handle events for dropzone
        //Visit http://www.dropzonejs.com/#events for more events
        vm.dzCallbacks = {
            'addedfile': function(file) {

                vm.addedFile = file;
            },
            'success': function(file, xhr) {

                // console.log('custom', file, xhr, xhr.data.resulttransaction);
                //console.log( 'data', xhr.data.resulttransaction);

                vm.resultransactions = xhr.data[0];
                vm.loading_register = false;
                notification.alert.show('Excelente!', 'Operación realizada exitosamente', 'success');

                // Remove file
                vm.dzMethods.removeFile(file);

                //vm.getTransaction();
            },
            'error': function(file, xhr) {
                vm.loading_register = false;
                notification.alert.show('Oops!', xhr.error_message, 'error');

                location.reload();
            },
            sending: function(file, xhr, form) {
                vm.loading_register = true;


                form.append('bank_description', vm.form.bankselect.banks.name);
                form.append('bank_id', vm.form.bankselect.banks.id);
                form.append('company_id', vm.companyId);


            }
        };
        vm.clearInputs = function() {
            location.reload();
            vm.form.today = {
                opened2: false,
            };
        };


        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.removeNewFile = function() {
            vm.dzMethods.removeFile(vm.addedFile);
        };

        vm.lockUnLocked = function(data) {
            vm.today = vm.form.transaction_date.toLocaleDateString();
            vm.types = vm.dateFormate(vm.today);
            vm.loadings = true;
            ImportingDataResource.lockUnLocked({
                    amount: vm.form.amount,
                    reference: vm.form.reference,
                    bank: vm.form.bankselect.banks.name,
                    today: vm.types,
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {
                vm.loadings = false;
                    if (response.status === 200) {
                        SweetAlert.swal({
                            title: "Excelente!",
                            text: 'Operación realizada correctamente!',
                            type: "success",
                        });
                        vm.clearInputs();
                        vm.getClear();
                    }
                }, function(response) {
                vm.loadings = false;
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives,
                        type: "error",
                    });
                   
                });
        }
        vm.UnLocked = function(data) {

            vm.today = vm.form.transaction_date.toLocaleDateString();
            vm.types = vm.dateFormate(vm.today);
            vm.loadings = true;
            ImportingDataResource.UnLocked({
                    amount: vm.form.amount,
                    reference: vm.form.reference,
                    bank: vm.form.bankselect.banks.name,
                    today: vm.types,
                    company_id: vm.companyId
                })
                .$promise.then(function(response) {
                    if (response.status === 200) {
                        vm.loadings = false;
                        SweetAlert.swal({
                            title: "Excelente!",
                            text: 'Operación realizada correctamente!',
                            type: "success",
                        });
                        vm.clearInputs();
                        vm.getClear();
                    }
                }, function(response) {
                vm.loadings = false;
                    SweetAlert.swal({
                        title: "Oops!",
                        text: response.data.motives,
                        type: "error",
                    });
                   

                });
        }

        vm.optionSelect = function() {
            if (angular.equals(vm.form.operationselect.code, 'masiveupload')) {
                vm.showImportDataBank = true;
                vm.showLockUnLockTrans = false;
            }

            if (angular.equals(vm.form.operationselect.code, 'lockunlock')) {
                vm.showImportDataBank = false;
                vm.showLockUnLockTrans = true;
            }
        };
        //  vm.getTransaction();
        /*----------------------------------------end--------------------------------------------*/


    }

    IDBController.$inject = [
        'system',
        'bankResource',
        'ApiUrl',
        '$auth',
        '$state',
        'notification',
        'restService',
        'ImportingDataResource',
        'SweetAlert',
        'securityResource',
        'localStorageService',
        '$location',
        'PermPermissionStore'
    ];

    angular
        .module('app')
        .controller('IDBController', IDBController);

})();