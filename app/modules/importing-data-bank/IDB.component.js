(function(){
    'use strict';
    
    angular
      .module('app')
      .component('idb', {
        templateUrl: '/app/modules/importing-data-bank/IDB.html',
        controller: 'IDBController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();