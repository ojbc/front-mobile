(function() {
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo Importar Data Banco.                        |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function searchadminController(system, bankResource, ApiUrl, $auth, $state, notification, restService, SweetAlert, securityResource, localStorageService, $location, PermPermissionStore, services, $scope, $timeout) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        vm.d = new Date();
        vm.loading_register = false;
        vm.hour = vm.d.toLocaleTimeString();
        vm.dd = vm.d.getDate();
        vm.mm = vm.d.getMonth() + 1;
        vm.yyyy = vm.d.getFullYear();
        vm.loading = false;
        if(typeof PermPermissionStore.getStore().superadmin  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        vm.form = {
            elements:"",
            bankselect:"",
            companyselect:"",
             
         }

        /*------------------------------------end---------------------------------------*/
        /*----------------------------------Functions-----------------------------------*/


        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.alls.query()
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listBanks = response.data;

                    vm.loading_banks = false;
                }
            });
        }
        vm.getCompany = function() {
            vm.loading_company = true;

            securityResource.company.all.query()
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listCompany = response.data;
                    vm.loading_company = false;
                }
            });
        }
        vm.getCompany();
        vm.getBanks();
        
        vm.clearInputs = function(form) {
            vm.form = {};
            form.$setPristine();
            vm.form = {
               elements:"",
               bankselect:"",
               companyselect:"",
                
            }
            
            vm.getBanks();
            vm.getCompany();
           vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
           
        }


        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/

        $scope.tempHtml = '';

       
        vm.dtOptions = {

            scrollX: true,
            destroy: true,
            cache: false,
            processing: true,
            serverSide: true,
            sorting: [[0, 'desc']],
            ajax: {
                url: ApiUrl + services.advancedsearch.searchadmin,
                type: "GET",
                data: function(d) {
                    d.elements=vm.form.elements;
                    if(vm.form.bankselect){
                        d.name_bank=vm.form.bankselect.name;
                    }
                    if(vm.form.companyselect){
                        d.company_id=vm.form.companyselect.id;
                    }
                    
                  
                },
                headers: {
                    'Authorization': 'Bearer ' + $auth.getToken()
                }
                
            },
            columns: [
            { data: "id"},
            { data: "transaction_date"},
            { data: "reference"},
            { data: "amount"},
            { data: "description"},
            
        ],
            language: {
                sLengthMenu:     "Mostrar  _MENU_",
                sZeroRecords:    "No se encontraron resultados",
                sEmptyTable:     "Ningún dato disponible en esta tabla",
                sInfo:           "Del _START_ al _END_",
                sInfoEmpty:      "0 Registros",
                sInfoFiltered:   "(filtrado de un total de _MAX_ Registros)",
                sInfoPostFix:    "",
                sSearch:         "Buscar: ",
                sUrl:            "",
                sInfoThousands:  ",",
                sLoadingRecords: "Cargando...",
                sProcessing: "Cargando",
                oPaginate: {
                    sFirst:    "Primero",
                    sLast:     "Último",
                    sNext:     "Siguiente",
                    sPrevious: "Anterior"
                },
                oAria: {
                    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sSortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
        };

        vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
       
        vm.search = function() {
            vm.loading = true;
           
            vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
            vm.loading = false;
           
        }
        vm.clear = function() {
            vm.loading = true;
           
            vm.dtManager = $('#dt_manager').DataTable(vm.dtOptions);
            vm.loading = false;
           
        }

        vm.clear();
        /*----------------------------------------end--------------------------------------------*/


    }

    searchadminController.$inject = [
        'system',
        'bankResource',
        'ApiUrl',
        '$auth',
        '$state',
        'notification',
        'restService',
        'SweetAlert',
        'securityResource',
        'localStorageService',
        '$location',
        'PermPermissionStore',
        'services',  
        '$scope', 
        '$timeout'
    ];

    angular
        .module('app')
        .controller('searchadminController', searchadminController);

})();