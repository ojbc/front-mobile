(function(){
    'use strict';
    
    angular
      .module('app')
      .component('searchadmin', {
        templateUrl: '/app/modules/search-admin/searchadmin.html',
        controller: 'searchadminController',
        controllerAs: 'vm',
        require: {
          // access to the functionality of the parent component called 'formComponent'
          parent: '^app'
        }
      });

})();