(function(){
    'use strict';

    angular
        .module('app')
        .component('association', {
            templateUrl: 'app/modules/association/association.html',
            controller: 'associationController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^app'
            }
        });

})();