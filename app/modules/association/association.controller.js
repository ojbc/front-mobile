(function(){
    'use strict';
    /*-------------------------------Module--------------------------------------
    |   Esta función tiene como finalidad realizar los métodos de               |
    | consulta, registro del módulo company.                                   |
    ------------------------------------end-------------------------------------*/
    /** @ngInject */
    function associationController(system, securityResource, SweetAlert, ModalService, $state, localStorageService, bankResource, $location, PermPermissionStore) {
        /*------------------------------------Var---------------------------------------*/
        var vm = this;
        // DEFAULTS
        vm.userId = localStorageService.get('auth_userid');
        vm.form = {
            bankselect: '',
            companyselect:''
        }
        vm.selected =true;
        vm.id= '';
        vm.loading = false;
        if(typeof PermPermissionStore.getStore().superadmin  !== 'undefined'){

        }else{
            $location.path('/home');
        }
        /*------------------------------------end---------------------------------------*/

        /*--------------------------------------Function_GET-------------------------------------*/
        vm.clearCompany = function(){
            vm.form.bankselect= "";
            vm.form.companyselect= "";
            vm.selected = true;
            vm.getBanks();
            vm.getCompany();
            vm.getAssociation();
        }
        vm.getBanks = function() {
            vm.loading_banks = true;

            bankResource.alls.query()
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listBanks = response.data;
                  
                    vm.loading_banks = false;
                }
            });
        }
        vm.getCompany = function() {
            vm.loading_company = true;

            securityResource.company.all.query()
                .$promise.then(function(response) {
                if(response.status === 200) {
                    vm.listCompany = response.data;
                    vm.loading_company = false;
                }
            });
        }
        vm.getAssociation = function() {
            vm.loading_companies = true;
            securityResource.company.allAssociation.query()
                .$promise.then(function(response) {
              
                if(response.status === 200) {
                    vm.listCompanies = response.data;
                    vm.loading_companies = false;
                }
            });
        }
        vm.selectAssociation = function(exc) {
                   
            vm.form.bankselect = exc.banks;
            vm.id =exc.id;
            vm.form.companyselect = exc.companies;
            vm.selected = false;
            
        }
       
        /*----------------------------------------end--------------------------------------------*/
        /*-------------------------------------Function_POST-------------------------------------*/
        vm.associationSelect = function()
        {
            vm.loading = true;
            securityResource.company.association(
                {
                    id: vm.id,
                    bank:vm.form.bankselect.id,
                    company:vm.form.companyselect.id
                   
                }
            )
                .$promise.then(function(response) {
                vm.loading = false;
                if (response.status === 200) {
                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });
                    vm.clearCompany();
                }else{
                    system.managerErrors(response);
                }
            }, function(response) {
                vm.loading = false;
                SweetAlert.swal({
                    title: "Oops!",
                    text: response.data.motives + '. Por favor intenta nuevamente!',
                    type: "error",
                });
            });
        }
        vm.association = function() {
            vm.loading = true;
            securityResource.company.association(
                {
                    bank:vm.form.bankselect.id,
                    company:vm.form.companyselect.id
                   
                }
            )
                .$promise.then(function(response) {
                vm.loading = false;
                if (response.status === 200) {
                    SweetAlert.swal({
                        title: "Excelente!",
                        text: 'Operación realizada correctamente!',
                        type: "success",
                    });
                    vm.clearCompany();
                }else{
                    system.managerErrors(response);
                }
            }, function(response) {
                vm.loading = false;
                SweetAlert.swal({
                    title: "Oops!",
                    text: response.data.motives + '. Por favor intenta nuevamente!',
                    type: "error",
                });
            });
        } 
        vm.statusBank = function(exc) {
            vm.loading_company = true;
            securityResource.company.statusBank({
              company: exc.id,
              status: !exc.status
              }).$promise.then(function(response) {
                vm.loading_company = false;
                if (response.status === 200) {
                }
              }, function(response) {
                vm.loading_company = false;
                  SweetAlert.swal({
                    title: "Oops!",
                    text: 'Algo salio mal, intenta nuevamente!',
                    type: "error",
                  });
            });
        }
        
        

        /*----------------------------------------end--------------------------------------------*/
        /*------------------------------------Function_Load--------------------------------------*/
        vm.getBanks();
        vm.getCompany();
        vm.getAssociation();
        /*----------------------------------------end--------------------------------------------*/
    }

    associationController.$inject = [
        'system',
        'securityResource',
        'SweetAlert',
        'ModalService',
        '$state',
        'localStorageService',
        'bankResource',
        '$location', 
        'PermPermissionStore'
    ];

    angular
        .module('app')
        .controller('associationController', associationController);

})();