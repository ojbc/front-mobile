(function () {
  'use strict';

  function runPermission($rootScope, $auth, PermPermissionStore, PermRoleStore, localStorageService, $log) {
    

    if ($auth.isAuthenticated()) {
        var permissions = [];

        angular.forEach(localStorageService.get('auth_permission'), function (value) {
          permissions.push(value.slug);
        });

        var rolselect = localStorageService.get('auth_role_obj');
        PermRoleStore.defineRole(rolselect.slug, permissions);

        angular.forEach(localStorageService.get('auth_permission'), function (value) {
          PermPermissionStore
            .definePermission(value.slug, function () {
              return true;
            });
        });

      
      } else {
        PermRoleStore.clearStore();
        PermPermissionStore.clearStore();

        
      }
  }

  runPermission.$inject = ['$rootScope', '$auth', 'PermPermissionStore', 'PermRoleStore', 'localStorageService', '$log'];

  angular
    .module('app')
    .run(runPermission);
})();
