(function(){
    'use strict';
    
    /** @ngInject */ 
    function runningDatatables(DTDefaultOptions){
        DTDefaultOptions.setLanguageSource('app/lang/datatables/es_ES.json')
            .setDisplayLength(10)
            .setBootstrapOptions({
                TableTools: {
                    classes: {
                        container: 'btn-group',
                        buttons: {
                            normal: 'btn btn-danger'
                        }
                    }
                },
                ColVis: {
                    classes: {
                        masterButton: 'btn btn-primary'
                    }
                }
            });
    }

    runningDatatables.$inject = ["DTDefaultOptions"];

    angular.module('app')
        .run(runningDatatables);
})();
