(function(){
  'use strict';

  /** @ngInject */
  function appController( system, $window, $auth, $rootScope, providers, authResource, localStorageService, angularLoad, $location, baseUrl, securityResource, ModalService, $stateParams, PermPermissionStore, PermRoleStore, $state, $timeout) {
    var vm = this;
    vm.userId = localStorageService.get('auth_userid');
    vm.username = localStorageService.get('auth_name');
      vm.print_pdf = false;
      vm.loadingTemplate = false;
    // vm.getListUsers();
    $rootScope.isAuth = vm.isAuth = function() {
      return $auth.isAuthenticated();
    };
   
    vm.logout = function() {
      // Ponemos a cargar el loader
      $rootScope.loader = true;
      //
      authResource.revokedAccess.save()
      .$promise.then(function(response){
        if(response.status === 200){
          // Cerramos la sesión
          $auth.logout();
          // Borramos el localStorage
          localStorageService.clearAll();
          // Refrescamos la pagina
          
          $location.path('/login');
        }
      },function(response){
        // Ponemos a cargar el loader
        $rootScope.loader = false;
        //system.errors.resources( response );
      });
    }
    angular.element(document).ready(function () {
      if ($auth.isAuthenticated()) {
        angularLoad.loadScript('assets/js/main.js').then(function() {
            $timeout(function() {
              App.init();
            }, 300);
        }).catch(function() {
            
        });
      }
    });
  }

  appController.$inject = [
    'system',
    '$window',
    '$auth',
    '$rootScope',
    'providers',
    'authResource',
    'localStorageService',
    'angularLoad',
    '$location',
    'baseUrl',
    'securityResource',
    'ModalService',
    '$stateParams',
    'PermPermissionStore', 
    'PermRoleStore',
    '$state',
    '$timeout'
  ];

  angular
    .module('app')
    .controller('appController', appController);
})();