(function () {
  'use strict';

  /** @ngInject */
  function registerPayResource($resource, restService, services) {

    return {
      all: restService.callServ.get(services.registerPay.all),
      invoice: restService.callServ.get(services.registerPay.invoice),
      paginate: restService.callServ.get(services.registerPay.paginate),
      todaysPaginates: restService.callServ.get(services.registerPay.todaysPaginates),
      totalPaginates: restService.callServ.get(services.registerPay.totalPaginates),
      save: function (form) {
        return restService.callServ.post(services.registerPay.save, form)
      },
      saveVoucher: function (form) {
        return restService.callServ.post(services.registerPay.saveVoucher, form)
      },
      searchCDN: function(form) {
        return restService.callServ.post(services.registerPay.searchCDN, form)
      },
      referenceID: function (form) {
            return restService.callServ.post(services.registerPay.referenceID, form)
      },
      searchVoucher: function (form) {
          return restService.callServ.post(services.registerPay.searchVoucher, form)
      },
        check: function (form) {
            return restService.callServ.post(services.registerPay.check, form)
        },
      total: function (form) {
          return restService.callServ.post(services.registerPay.total, form)
      },
      today: function (form) {
          return restService.callServ.post(services.registerPay.today, form)
      },
        printPdfMD: function(form) {
          return restService.callServ.post(services.registerPay.printPdfMD, form)
      },
      printPdfToday : function(form) {
          return restService.callServ.post(services.registerPay.printPdfToday, form)
      },
      searchCDNData : function(form) {
        return restService.callServ.post(services.registerPay.searchCDNData, form)
      },
      payments: restService.callServ.get(services.registerPay.payments),
      referenceInfo: restService.callServ.get(services.registerPay.referenceInfo)
      
    };
  }

  registerPayResource.$inject = [
    '$resource',
    'restService',
    'services'
  ];

  angular.module('app')
    .factory('registerPayResource', registerPayResource);
})();
