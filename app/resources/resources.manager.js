(function(){
    'use strict';

    /** @ngInject */
    function managerResource( $resource, restService, services ){

        return {
            all: restService.callServ.get( services.manager.all ),
            save: function ( form ) {
                return restService.callServ.post( services.manager.save, form )
            },
            delete: function( form ) {
                return restService.callServ.post( services.manager.delete, form )
            },
            addGiftByInvoice: function( form ) {
                return restService.callServ.post( services.manager.addGiftByInvoice, form )
            },
            deleteGiftByInvoice: function(form) {
              return restService.callServ.post(services.manager.deleteGiftByInvoice, form)
            },
            giftsByInvoice: restService.callServ.get( services.manager.giftsByInvoice ),
            getNoteGifts: restService.callServ.get( services.manager.getNoteGifts ),
            searchByEmail: function (form) {
              return restService.callServ.post(services.manager.searchByEmail, form)
            },
            documentManager:  function ( form ) {
                return restService.callServ.post( services.manager.documentManager, form )
            }
        };
    }

    managerResource.$inject = [
    	'$resource',
    	'restService',
        'services'
    ];

    angular.module('app')
        .factory('managerResource',managerResource);
})();
