(function(){
    'use strict';

    /** @ngInject */
    function analystsResource( $resource, restService, services ){

        return {
            
            all: restService.callServ.get( services.analysts.all ),
            users: restService.callServ.get( services.analysts.users ),
            analyst: restService.callServ.get( services.analysts.analyst ),
            associatedManagerLists: restService.callServ.get( services.analysts.associatedManagerLists ),
            save: function (form) {
            return restService.callServ.post(services.analysts.save, form)
            },
            delete: function (form) {
            return restService.callServ.post(services.analysts.delete, form)
            }
            
        };
    }

    analystsResource.$inject = [
    	'$resource',
    	'restService',
        'services'
    ];

    angular.module('app')
        .factory('analystsResource',analystsResource);
})();
