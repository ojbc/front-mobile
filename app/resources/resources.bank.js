(function(){
    'use strict';

    /** @ngInject */
    function bankResource( $resource, restService, services ){

        return {
            all: restService.callServ.get( services.bank.all ),
            alls: restService.callServ.get( services.bank.alls ),
            save: function( form ) {
                return restService.callServ.post( services.bank.save, form )
            },
            delete: function( form ) {
                return restService.callServ.post( services.bank.delete, form )
            }
        };
    }

    bankResource.$inject = [
    	'$resource',
    	'restService',
        'services'
    ];

    angular.module('app')
        .factory('bankResource',bankResource);
})();
