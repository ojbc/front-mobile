(function () {
    'use strict';

    /** @ngInject */
    function preloadsResource($resource, restService, services) {

        return {
            all: restService.callServ.get( services.preloads.all )

        };
    }

    preloadsResource.$inject = [
        '$resource',
        'restService',
        'services'
    ];

    angular.module('app')
        .factory('preloadsResource', preloadsResource);
})();