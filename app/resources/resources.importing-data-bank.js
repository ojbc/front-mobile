(function(){
    'use strict';

    /** @ngInject */
    function ImportingDataResource( $resource, restService, services ){

        return {
            all: restService.callServ.get( services.Importing.all ),
            lockUnLocked: function (form) {
                return restService.callServ.post(services.Importing.lockUnLocked, form)
            },
            UnLocked: function (form) {
                return restService.callServ.post(services.Importing.UnLocked, form)
            },
            deleteTransaction: function (form) {
                return restService.callServ.post(services.Importing.deleteTransaction, form)
            },
            desData: function(form) {
                return restService.callServ.post(services.registerPay.upData, form)
            },
        };
    }

    ImportingDataResource.$inject = [
        '$resource',
        'restService',
        'services'
    ];

    angular.module('app')
        .factory('ImportingDataResource',ImportingDataResource);
})();