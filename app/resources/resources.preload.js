(function () {
    'use strict';

    /** @ngInject */
    function preloadResource($resource, restService, services) {

        return {
            paginate: restService.callServ.get(services.preload.paginate),
            save: function (form) {
                return restService.callServ.post(services.preload.save, form)
            },
            delete: function (form) {
                return restService.callServ.post(services.preload.delete, form)
            },
            checkPreload: function (form) {
                return restService.callServ.post(services.preload.checkPreload, form)
            },
            printPdfap: function (form) {
                return restService.callServ.post(services.preload.printPdfap, form)
            },
            printPdfnp: function (form) {
                return restService.callServ.post(services.preload.printPdfnp, form)
            },
            deletelist: function (form) {
                return restService.callServ.post(services.preload.deletelist, form)
            }


        };
    }

    preloadResource.$inject = [
        '$resource',
        'restService',
        'services'
    ];

    angular.module('app')
        .factory('preloadResource', preloadResource);
})();