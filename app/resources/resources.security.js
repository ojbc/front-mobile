(function(){
    'use strict';

    /** @ngInject */
    function securityResource( $resource, restService, services ){
        return {
            users: {
                profiles: restService.callServ.get(services.security.users.profiles),
                all: restService.callServ.get( services.security.users.all ),
                consulta: restService.callServ.get( services.security.consulting.paginate ),
                save: function( form ) {
                    return restService.callServ.post( services.security.users.save, form )
                },
                changeRole: function( form ) {
                    return restService.callServ.post( services.security.users.changeRole, form )
                },
                updatePermission: function( form ) {
                    return restService.callServ.post( services.security.roles.togglePermission, form )
                },
                delete: function( form ) {
                    return restService.callServ.post( services.security.users.delete, form )
                },
                saveUpdate: function( form ) {
                    return restService.callServ.post( services.security.users.saveUpdate, form )
                },
                resetPassword: function( form ) {
                    return restService.callServ.post( services.security.users.resetPassword, form )
                },
                status: function( form ) {
                    return restService.callServ.post( services.security.users.status, form )
                },
                getFullDetails: restService.callServ.get( services.security.users.getFullDetails ),
                allUserCompany: restService.callServ.get( services.security.users.allUserCompany )
            },
            roles: {
                all: restService.callServ.get( services.security.roles.all ),
                getPermission: restService.callServ.get( services.security.roles.getPermission ),
                save: function( form ) {
                    return restService.callServ.post( services.security.roles.save, form )
                },
                updatePermission: function( form ) {
                    return restService.callServ.post( services.security.roles.togglePermission, form )
                }
            },
            permission: {
                all: restService.callServ.get( services.security.permission.all ),
                save: function( form ) {
                    return restService.callServ.post( services.security.permission.save, form )
                }
            },
            consulting: {
                all: restService.callServ.get( services.security.consulting.all )
            },
            company:{
                all: restService.callServ.get( services.security.company.all ),
                allUsers: restService.callServ.get( services.security.company.allUsers ),
                allAssociation: restService.callServ.get( services.security.company.allAssociation ),
                save:function( form ) {
                    return restService.callServ.post( services.security.company.save, form )
                },
                association:function( form ) {
                    return restService.callServ.post( services.security.company.association, form )
                },
                status:function( form ) {
                    return restService.callServ.post( services.security.company.status, form )
                }, 
                statusBank:function( form ) {
                    return restService.callServ.post( services.security.company.statusBank, form )
                }
            }

        };
    }

    securityResource.$inject = [
    	'$resource',
    	'restService',
        'services'
    ];

    angular.module('app')
        .factory('securityResource',securityResource);
})();
