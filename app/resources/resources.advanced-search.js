(function () {
    'use strict';

    /** @ngInject */
    function advancedsearchResource($resource, restService, services) {

        return {
            search: restService.callServ.get( services.advancedsearch.search ),
            searchadmin: restService.callServ.get( services.advancedsearch.searchadmin )

        };
    }

    advancedsearchResource.$inject = [
        '$resource',
        'restService',
        'services'
    ];

    angular.module('app')
        .factory('advancedsearchResource', advancedsearchResource);
})();