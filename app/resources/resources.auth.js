(function(){
    'use strict';

    /** @ngInject */
    function authResource( $resource, providers, restService, services ){

        return {
            revokedAccess: $resource( providers.oauthServer.baseUrl + services.auth.revokedAccess )
        };
    }

    authResource.$inject = [
    	'$resource',
    	'providers',
    	'restService',
        'services'
    ];

    angular.module('app')
        .factory('authResource',authResource);
})();
