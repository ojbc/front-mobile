(function(){
    'use strict';

    /** @ngInject */
    function userResource( $resource, restService, services){
        return {
            profiles: restService.callServ.get(services.users.profiles),
          save: function( form ) {
                return restService.callServ.post(services.users.findUsers, form)
            },

        };
    }

  userResource.$inject = [
      '$resource',
      'restService',
      'services'
    ];

    angular.module('app')
        .factory('userResource', userResource);
})();
