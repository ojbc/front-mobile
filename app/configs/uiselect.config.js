(function () {
  'use strict';

  function uiSelectDefaultConfig(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
  }

  uiSelectDefaultConfig.$inject = ['uiSelectConfig'];

  angular.module('app')
    .config(uiSelectDefaultConfig);

})();