(function(){
    'use strict';

    /** @ngInject */
    function satellizerProviders($authProvider,providers){
        $authProvider.httpInterceptor = function() { return true; };
        $authProvider.withCredentials = false;
        $authProvider.tokenRoot = null;
        $authProvider.baseUrl = providers.oauthServer.baseUrl;
        $authProvider.loginUrl = '/access';
        $authProvider.signupUrl = '/auth/signup';
        $authProvider.unlinkUrl = '/auth/unlink/';
        $authProvider.tokenName = 'token';
        $authProvider.tokenPrefix = 'satellizer';
        $authProvider.tokenHeader = 'Authorization';
        $authProvider.tokenType = 'Bearer';
        $authProvider.storageType = 'sessionStorage';
    }

    /** @ngInject */
    function localStorageProviders(localStorageServiceProvider){
        localStorageServiceProvider
            .setPrefix('tendencias')
            .setStorageType('sessionStorage')
            .setNotify(true, true);
    }

    /** @ngInject */
    function nyaBsProvider(nyaBsConfigProvider) {
        nyaBsConfigProvider
            .setLocalizedText({
                defaultNoneSelection: 'Selecciona una opción',
                noSearchResult: 'No encontrado',
                numberItemSelected: '%d item seleccionados',
                selectAll: 'Marcar todo',
                deselectAll: 'Desmarcar todo'
            });
    }

    angular.module('app')
        .config(satellizerProviders)
        .config(localStorageProviders)
        .config(nyaBsProvider)

})();

