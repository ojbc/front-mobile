
(function(){
    'use strict';

    /** @ngInject */
    function systemService( $timeout, SweetAlert ){
        var vm = this;

        var notify = {
            show: function ( params ) {
              // 
              $timeout(function() {
                jQuery.gritter.add(params);
              });
              
              return false;
            },
            error: function( text, title ) {
              this.show({
                title: title ? title : 'Oops algo salio mal',
                text: text,
                image: config.assetsPath + '/' +  config.imgPath + '/close-icon.png',
                class_name: 'color youtube',
                time: 2000
              });
            }
        }

        var alert = function() {
          return {
            show: function ( options, callback) {
              return SweetAlert.swal( options, callback );
            },
            error:function ( text ) {
              return SweetAlert.swal({
                title: "Oops!",
                text: angular.isDefined(text) ? text : 'Ha ocurrido un error',
                type: "error",
              });
            },
            success: function ( text ) {
              return SweetAlert.swal({
                title: "Excelente!",
                text: text,
                type: "success",
              });
            }
          }
        };

        var confirm = function( text, callback, btn_text ) {
          SweetAlert.swal({
               title: "¿Estas seguro?",
               text: text,
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: angular.isDefined(btn_text) ? btn_text : "Si, borrar!",
               closeOnConfirm: false
            }, callback);
        }
         var confirmPay = function( text, callback, btn_text ) {
          SweetAlert.swal({
               title: "¿Estas seguro?",
               text: text,
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: angular.isDefined(btn_text) ? btn_text : "¡Si, pagar!",
               closeOnConfirm: false
            }, callback);
        }

        var managerErrors = function( response ) {
          if( response.status === 400 || response.status === 500) {
            SweetAlert.swal({
              title: "Oops!",
              text: 'No tuvimos buena respuesta del servidor, Intente nuevamente y si el problema persiste por Favor Contacta con el Departamento de Tecnología de la Información',
              type: "error",
            });
          }

          if( response.status === 401 ) {
              angular.forEach( response.data.motives, function (value, key) {
                  notify.error(value, "Oops!");
              });
          }

          

          if( response.error == 'Autenticate' ) {              
              // Cerramos la sesión
              $auth.logout();
              // Borramos el localStorage
              localStorageService.clearAll();
              // Mandamos al login
              $location.path('/login');
              // Mostramos el aviso
              notify.error('La sessión ha expirado.', "Oops!");
          }
        }

        // Instanciamos
        vm.notify = notify;
        vm.alert = alert;
        vm.confirm = confirm;
        vm.confirmPay = confirmPay;
        vm.managerErrors = managerErrors;
    }

    systemService.$inject = [
        '$timeout',
        'SweetAlert'
    ];

    angular.module('app')
        .service('system',systemService);
})();
