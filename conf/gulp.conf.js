'use strict';

/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

const path = require('path');
const gutil = require('gulp-util');

exports.ngModule = 'app';

/**
 *  The main paths of your project handle these with care
 */
exports.paths = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  tasks: 'gulp_tasks',
  const: 'constants'
};

/**
* used on gulp dist
*/
exports.htmlmin = {
  ignoreCustomFragments: [/{{.*?}}/]
};

exports.path = {};
for (const pathName in exports.paths) {
  if (Object.prototype.hasOwnProperty.call(exports.paths, pathName)) {
    exports.path[pathName] = function () {
      const pathValue = exports.paths[pathName];
      const funcArgs = Array.prototype.slice.call(arguments);
      const joinArgs = [pathValue].concat(funcArgs);
      return path.join.apply(this, joinArgs);
    };
  }
}

/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.errorHandler = function (title) {
  return function (err) {
    gutil.log(gutil.colors.red(`[${title}]`), err.toString());
    this.emit('end');
  };
};
/**
 *  Wiredep is the lib which inject bower dependencies in your project
 *  Mainly used to inject script tags in the index.html but also used
 *  to inject css preprocessor deps and js files in karma
 */
exports.wiredep = {
  exclude: [/\/bootstrap\.js$/, /\/bootstrap-sass\/.*\.js/, /\/bootstrap\.css/],
  directory: 'bower_components',
  dependencies: true,
  devDependencies: false,
  fileTypes: {
    html: {
      block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
      detect: {
        js: /<script.*src=['"]([^'"]+)/gi,
        css: /<link.*href=['"]([^'"]+)/gi
      },
      replace: {
        css: function(filePath) {
          if (filePath.indexOf(".min") === -1) {
            var minFilePath = filePath.replace('.css', '.min.css');
            var fullPath = path.join(__dirname, minFilePath);            
            if (!fs.existsSync(fullPath)) {
              return '<link rel="stylesheet" href="' + filePath + '" />';
            } else {
              return '<link rel="stylesheet" href="' + minFilePath + '" />';
            }
          } else {
            return '<link rel="stylesheet" href="' + filePath + '" />';
          }
        },
        js: function(filePath) {
          if (filePath.indexOf(".min") === -1) { // isProduction is a variable that is set to determine build type. Its good practise to use unminified files during development 
            var minFilePath = filePath.replace('.js', '.min.js');
            var fullPath = path.join(__dirname, minFilePath);
            if (!fs.existsSync(fullPath)) {
              return '<script src="' + filePath + '"></script>';
            } else {
              return '<script src="' + minFilePath + '"></script>';
            }
          } else {
            return '<script src="' + filePath + '"></script>';
          }
        }
      },
    }
  }
};

