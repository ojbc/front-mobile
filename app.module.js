(function(){
  'use strict';

angular
  .module('app', [
    'build.constants',
    'build.services',
    'ui.router', 
    'ui.router.state.events',
    'permission',
    'permission.ui',
    'ngResource', 
    'satellizer',
    'LocalStorageModule',
    'angularLoad',
    'ui.bootstrap',
    'angularModalService',
    'nya.bootstrap.select',
    'oitozero.ngSweetAlert',
    'slugifier',
    'datatables',
    'ui.select',
    'ngSanitize',
    'ngMessages',
    'thatisuday.dropzone',
    //'ui.utils.masks',
    //'ui.mask',
    'ui.sortable',
    'ui.tree',
    'angular.filter',
    'oc.lazyLoad'
  ])
  .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
      modules: [
        { 
          name: 'AssociationModule',
          files: [
            'app/modules/association/association.component.js',
            'app/modules/association/association.controller.js',
            'app/resources/resources.security.js',
            'app/resources/resources.bank.js',
          ]
        },
        {
          name: 'CompanyModule',
          files: [
            'app/modules/company/company.component.js',
            'app/modules/company/company.controller.js',
            'app/resources/resources.security.js',
          ]
        },
        {
          name: 'DeleteTransactionModule',
          files: [
            'app/modules/delete-transaction/DT.component.js',
            'app/modules/delete-transaction/DT.controller.js',
            'app/resources/resources.security.js',
            'app/resources/resources.bank.js',
            'app/resources/resources.importing-data-bank.js',
          ]
        },
        {
          name: 'ImportingDataBankModule',
          files: [
            'app/modules/importing-data-bank/IDB.component.js',
            'app/modules/importing-data-bank/IDB.controller.js',
            'app/resources/resources.security.js',
            'app/resources/resources.bank.js',
            'app/resources/resources.importing-data-bank.js',
          ]
        },
        {
          name: 'ListPaymentModule',
          files: [
            'app/modules/list-payment/listpay.component.js',
            'app/modules/list-payment/listpay.controller.js',
          ]
        },
        {
          name: 'PasswordModule',
          files: [
            'app/modules/password/password.component.js',
            'app/modules/password/password.controller.js',
            'app/resources/resources.security.js',
          ]
        },
        {
          name: 'PreloadModule',
          files: [
            'app/modules/preload/preload.component.js',
            'app/modules/preload/preload.controller.js',
            'app/modules/preload/report/list/list.controller.js',
            'app/resources/resources.bank.js',
            'app/resources/resources.preload.js',
            'app/resources/resources.preloads.js',
          ]
        },
        {
          name: 'ProfileModule',
          files: [
            'app/modules/profile/profile.component.js',
            'app/modules/profile/profile.controller.js',
            'app/resources/resources.security.js',
          ]
        },
        {
          name: 'RegisterPayModule',
          files: [
            'app/modules/register-pay/register-pay.component.js',
            'app/modules/register-pay/register-pay.controller.js',
            'app/modules/register-pay/register-pay.controller.js',
            'app/modules/register-pay/pay/description/description.controller.js',
            'app/modules/register-pay/pay/DNI/DNI.controller.js',
            'app/modules/register-pay/pay/find/find.controller.js',
            'app/resources/resources.register-pay.js',
            'app/resources/resources.importing-data-bank.js',
            'app/resources/resources.bank.js',
          ]
        },
        {
          name: 'TodayModule',
          files: [
            'app/modules/report/today/BC.component.js',
            'app/modules/report/today/BC.controller.js',
            'app/resources/resources.register-pay.js',
          ]
        },
        {
          name: 'TotalModule',
          files: [
            'app/modules/report/total/MD.component.js',
            'app/modules/report/total/MD.controller.js',
            'app/resources/resources.security.js',
            'app/resources/resources.analysts.js',
            'app/resources/resources.register-pay.js',
          ]
        },
        {
          name: 'SearchModule',
          files: [
            'app/modules/search/search.component.js',
            'app/modules/search/search.controller.js',
            'app/resources/resources.bank.js',

          ]
        },
        {
          name: 'SearchAdminModule',
          files: [
            'app/modules/search-admin/searchadmin.component.js',
            'app/modules/search-admin/searchadmin.controller.js',
            'app/resources/resources.bank.js',
            'app/resources/resources.security.js',

          ]
        },
        {
          name: 'TransactionCompanyModule',
          files: [
            'app/modules/transaction-company/TC.component.js',
            'app/modules/transaction-company/TC.controller.js',
            'app/resources/resources.bank.js',
            'app/resources/resources.importing-data-bank.js',

          ]
        },
        {
          name: 'ConsultingModule',
          files: [
            'app/modules/security/consulting/consulting.controller.js',
            'app/modules/security/consulting/consulting.component.js',
          ]
        },
        {
          name: 'PermissionsModule',
          files: [
            'app/modules/security/permissions/permissions.controller.js',
            'app/modules/security/permissions/permissions.component.js',
            'app/modules/security/permissions/modals/add/adding.controller.js',
          ]
        },
        {
          name: 'RolesModule',
          files: [
            'app/modules/security/roles/roles.controller.js',
            'app/modules/security/roles/roles.component.js',
            'app/modules/security/roles/modals/add/adding.controller.js',
          ]
        },
        {
          name: 'UsersModule',
          files: [
            'app/modules/security/users/users.controller.js',
            'app/modules/security/users/users.component.js',
            'app/modules/security/users/modals/add/adding.controller.js',
          ]
        },
        {
          name: 'UsersDetailsModule',
          files: [
            'app/modules/security/users/detail/details.component.js',
            'app/modules/security/users/detail/details.controller.js',
            'app/resources/resources.manager.js',
          ]
        }
      ]
    });
  }]);
})();


