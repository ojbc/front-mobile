angular.module("build.constants", [])
.constant("baseUrl", "http://verificapagos.co")
.constant("ApiUrl", "http://verificapagosapi.co/api")
.constant("providers", {
  "oauthServer": {
    "baseUrl": "http://verificapagosapi.co",
    "clientid": "2"
  }
});
